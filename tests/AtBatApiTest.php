<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\AtBatApiController;
use App\Models\Game;
use App\Models\Batter;


class AtBatApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $game = Game::findOrFail(23);
        $pitcher = Batter::findOrFail(3510);
        AtBatApiController::sendExceededNotification($game,$pitcher,'oordoso@gmail.com','TOTAL');
    }
}
