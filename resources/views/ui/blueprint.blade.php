{!! Html::style(LinkHelper::publicResource('/libs/blueprint/screen.css'),['media'=>'screen, projection']) !!}
{!! Html::style(LinkHelper::publicResource('/libs/blueprint/print.css'),['media'=>'print']) !!}
<!--[if lt IE 8]>
{!! Html::style(LinkHelper::publicResource('/libs/blueprint/ie.css'),['media'=>'screen, projection']) !!}
<![endif]-->
