@extends('game.update')

@section('header-part')
    {!! Html::style(LinkHelper::publicResource('/css/update/atbat.css')) !!}
@stop

@section('content-form')
    @include('errors.list')
@stop