<?php
use \App\Models\Batter;


$teamType = ['visitor','home'];
//dd($game,$lineupPlayers,$settings['usePitchTracker']);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
    {!! Html::style(LinkHelper::publicResource('/libs/jquery-scrollbar/jquery.jscrollpane.css')) !!}
    {!! Html::style(LinkHelper::publicResource('/css/ui.css')) !!}
    {!! Html::style(LinkHelper::publicResource('/css/update/atbat/atbat_.css')) !!}
    {!! Html::style(LinkHelper::publicResource('/css/update/atbat/score_editor.css')) !!}
    {!! Html::style(LinkHelper::publicResource('/css/update/atbat/field.css')) !!}
    {!! Html::style(LinkHelper::publicResource('/css/update/scorepad_cell.css')) !!}
    {!! Html::style(LinkHelper::publicResource('/css/update/warning.css')) !!}
</head>
<body>
{!! Html::script(LinkHelper::publicResource('/libs/jquery/js/jquery-2.1.4.min.js')) !!}
{!! Html::script(LinkHelper::publicResource('/libs/jquery-ui/jquery-ui.min.js')) !!}
{!! Html::script(LinkHelper::publicResource('/libs/underscore/underscore.js')) !!}
{!! Html::script(LinkHelper::publicResource('/libs/moment/moment.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/crossbrowser.js')) !!}
{!! Html::script(LinkHelper::publicResource('/libs/jquery-scrollbar/jquery.jscrollpane.min.js')) !!}
{!! Html::script(LinkHelper::publicResource('/libs/jquery-scrollbar/jquery.mousewheel.js')) !!}
{!! Html::script(LinkHelper::publicResource('/libs/file-saver/FileSaver.min.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/update/warning.js')) !!}
<div class="ui-header">
    <img src="{{url('/images/atbat/Northwoods-League-Logo.jpg')}}" class="ui-image-logo">
    <div style="margin-top: 130px;">
        @include('auth.user')
    </div>
</div>
<div class="ui-sub-header">
    <span > Score Game &ndash; {{$game->teamVisitor->Name}} at {{$game->teamHome->Name}} </span>
</div>
@include('errors.list')
@if(isset($lineupPlayers))
<div class="js-header-second ui-sub-header-second">
</div>
<div class="ui-container">

    <div>
        <div class="ui-statistic-div">
            <span class="ui-statistic-title" >{{$game->teamVisitor->Name}} (0-0) at {{$game->teamHome->Name}} (0-0)</span>
            <table class="ui-table-statistic" cellpadding="3px" cellspacing="0">
                <tr>
                    <th colspan="2" class="ui-table-statistic-head-color" selected="1">{{$game->teamVisitor->getSimpleName()}}</th>
                    <th class="ui-table-statistic-head"></th>
                    @for($i=1;$i<=10;$i++)
                        <th class="ui-table-statistic-head js-inning-visitor-head">{{$i}}</th>
                    @endfor
                    <th class="ui-table-statistic-head" style="padding-left:20px">R</th>
                    <th class="ui-table-statistic-head">H</th>
                    <th class="ui-table-statistic-head">E</th>
                    <th colspan="2" class="ui-table-statistic-head-color">{{$game->teamHome->getSimpleName()}}</th>
                </tr>
                <tr>
                    <td rowspan="2">
                        @if(!empty($game->teamVisitor->logo))
                            <img style="width:90px" src="{{url($game->teamVisitor->logo)}}">
                        @endif
                    </td>
                    <td rowspan="2" class="js-inning-visitor-r ui-points">0</td>
                    <td class="ui-table-statistic-body">{{$game->teamVisitor->Abv}}</td>
                    @for($i=1;$i<=10;$i++)
                        <td  class="js-inning-visitor ui-table-statistic-body-color" inning="{{$i}}"></td>
                    @endfor
                    <td class="js-inning-visitor-r ui-table-statistic-body" style="padding-left:20px">0</td>
                    <td class="js-inning-visitor-h ui-table-statistic-body">0</td>
                    <td class="js-inning-visitor-e ui-table-statistic-body">0</td>
                    <td rowspan="2" class="js-inning-home-r ui-points">0</td>
                    <td rowspan="2">
                        @if(!empty($game->teamHome->logo))
                            <img style="width:90px" src="{{url($game->teamHome->logo)}}">
                        @endif
                    </td>
                </tr>
                <tr>
                    <td class="ui-table-statistic-body">{{$game->teamHome->Abv}}</td>
                    @for($i=1;$i<=10;$i++)
                        <td  class="js-inning-home ui-table-statistic-body-color" inning="{{$i}}"></td>
                    @endfor
                    <td class="js-inning-home-r ui-table-statistic-body" style="padding-left:20px">0</td>
                    <td class="js-inning-home-h ui-table-statistic-body">0</td>
                    <td class="js-inning-home-e ui-table-statistic-body">0</td>
                </tr>
            </table>
        </div>
        <div class="ui-weather-div" style="">
            <div class="ui-stadium-name">
                <span>{{$game->teamHome->location}}</span>
            </div>
            <div class="ui-weather">
                <img src="{{url($game->weatherIcons[$game->weather])}}" class="ui-weather-icon">
                <span class="ui-weather-text"> {{$game->temperature.'° '.$game->weather}}</span>
            </div>
            <div>
                <div class="ui-button-start js-button-game-action">
                    <div class="icon"></div>
                    <div class="text"></div>
                    <a class="hidden" href="{{action('GameController@edit',$game->idgame)}}"></a>
                </div>
            </div>
        </div>
    </div>

    <div>
        <div class="ui-field-container">
            <div class="js-field ui-field-div">
                <canvas class="js-field-canvas"  width="700" height="340"></canvas>
                <img src="{{url('/images/atbat/img_baseball_field.png')}}" class="ui-baseball-field">
                <div class="ui-field-bases js-field-bases">
                    <div class="base" base="1"></div>
                    <div class="base" base="2"></div>
                    <div class="base" base="3"></div>
                    <div class="base" base="4"></div>
                </div>
            </div>
            <div class="ui-field-label js-label-field"></div>

            <div class="js-batting-batside ui-batting-batside">
                {{--BATTING RIGHT<span class="ui-triangle" type="right"></span>--}}
                {{--<span class="ui-triangle" type="left"></span>BATTING LEFT--}}
            </div>

            <div class="ui-container-game-controls">
                <div class="ui-field-button js-button-game-undo">
                    <span style="top: -6px; left: 15px;">&#8630;</span>
                </div>
                <div class="ui-field-button js-button-game-next" style="display: none">
                    <span style="top: -11px; left: 14px;">&#8594;</span>
                </div>
            </div>


            <div class="ui-deck-div">
                <span class="ui-deck-text">
                   <span><span style="color: #afafaf;">AT BAT: </span><span class="js-label-atbat"></span></span>
                    <span style="display: block; padding-top: 10px"><span style="color: #afafaf;">ON DECK: </span><span class="js-label-ondeck"></span></span>
                </span>
            </div>

            <div class="ui-bso-div">
                <div style=" display: table; margin: auto;">
                    <div class="js-pitch-type ui-bso-part" type="ball">
                        <span>B</span>
                        <div class="ui-circle" value="1"></div>
                        <div class="ui-circle" value="2"></div>
                        <div class="ui-circle" value="3"></div>
                        <div class="ui-circle" value="4"></div>
                    </div>
                    <div class="js-pitch-type ui-bso-part" type="strike">
                        <span>S</span>
                        <div class="ui-circle" value="1"></div>
                        <div class="ui-circle" value="2"></div>
                        <div class="ui-circle" value="3"></div>
                    </div>
                    <div class="js-pitch-type ui-bso-part" type="out" style=" margin-right: 0px;">
                        <span>O</span>
                        <div class="ui-circle"></div>
                        <div class="ui-circle"></div>
                        <div class="ui-circle"></div>
                    </div>
                </div>
            </div>

            @if ($settings['usePitchTracker'])
            <div class="ui-pitch-region">
                <div class="js-pitch-region" style="height: 175px;">
                    <img src="{{url('/images/atbat/img_pitch_trecking.png')}}" style="width: 100%"/>
                </div>
                <div class="ui-button-pitch js-button-pitch">
                    Pitch
                </div>
            </div>
            @endif
        </div>
        <div class="ui-buttons-container">
            <table>
                <tr>
                    <td><div class="ui-field-button js-button-field-strike-looking">Strike Looking</div></td>
                    <td><div class="ui-field-button js-button-field-ball">Ball</div></td>
                    <td rowspan="2">
                        <div class="ui-field-ballinplay js-button-pitch-menu">
                            <div class="ui-field-ballinplay-circle"></div>
                            <div>Ball In Play</div>
                        </div>
                    </td>
                    <td><div class="ui-field-button js-button-field-hitbypitch">Hit by Pitch</div></td>
                    <td><div class="ui-field-button js-button-field-error">Error</div></td>
                </tr>
                <tr>
                    <td><div class="ui-field-button js-button-field-strike-swinging">Strike Swinging</div></td>
                    <td><div class="ui-field-button js-button-field-ball-foul">Foul Ball</div></td>
                    <td><div class="ui-field-button js-button-field-stealing">Stealing</div></td>
                    <td><div class="ui-field-button js-button-field-out-passedball">Passed Ball</div></td>
                </tr>
            </table>
        </div>

        <div>
            <table class="ui-buttons-div">
                <tr>
                    <td><div class="ui-field-button js-button-field-out-catcherinf">Catcher Inf</div></td>
                    <td><div class="ui-field-button js-button-field-balk">Balk</div></td>
                    <td><div class="ui-field-button js-button-field-out-wildpitch">Wild Pitch</div></td>
                    <td><div class="ui-field-button">Rare 4</div></td>
                    <td><div class="ui-field-button">Rare 5</div></td>
                    <td><div class="ui-field-button">Rare 6</div></td>
                </tr>
            </table>
        </div>

        <div class="ui-buttons-div hidden">
            <table cellspacing="10" width="100%" style="table-layout:fixed;">
                <tr style="background-color: #e89f02; text-align: center; vertical-align: middle; height:36px">
                    <td class="js-button-hit" disabled="1">HIT</td>
                    <td class="js-button-k" disabled="1">K</td>
                    <td class="js-button-outs" disabled="1">OUTS</td>
                    <td class="js-button-err" disabled="1">ERR</td>
                    <td class="js-button-steal" disabled="1">MISC</td>
                    <td class="js-button-ar" disabled="1">AR</td>
                </tr>
            </table>
        </div>

    </div>

    <div class="ui-tables-bottom-div">
        <div class="ui-tables-left-div js-table-left-div">
            <div class="ui-container-div-title">
                <div class="js-button-container ui-button-container" style="margin-left: 15px"selected="selected" type="lineup">Lineup</div>
                <div class="js-button-container ui-button-container" style="margin-left: 30px" type="scorepad">Score Pad</div>
            </div>
            <div class="ui-lineup-div-title">
                <div class="js-button-lineup ui-button-lineup" selected="selected" team="visitor" type="batters">{{$game->teamVisitor->getSimpleName()}}</div>
                <div class="js-button-lineup ui-button-lineup" style="margin-left: -4px" team="home" type="batters">{{$game->teamHome->getSimpleName()}}</div>
            </div>
            <div class="js-container-lineup hidden">
                @foreach($lineupPlayers as $lineupTeam)
                    <div class="js-lineup ui-command-div hidden" type="batters">
                        <div class="ui-caption-command">{{$lineupTeam['name']}}</div>
                        <table class="ui-table-command">
                            <tr>
                                <th colspan="2" class="ui-table-command-header">Lineup</th>
                                <th>AB</th>
                                <th>R</th>
                                <th>H</th>
                                <th>RBI</th>
                                <th>BB</th>
                                <th>SO</th>
                            </tr>
                            @foreach ($lineupTeam['lineup'] as $batter)

                                <tr class="js-lineup-batter" batter="{{$batter->BatterPosition}}" player="{{$batter->idbatter}}">
                                    <td>{{$batter->Number}}</td>
                                    <td class="ui-table-command-player" {{$batter->SubOrder>0? 'substitution="true"':''}}>{{$batter->player->getCutName().' '.Batter::$defensePositions[$batter->DefensePosition]}}</td>
                                    <td type="AB">0</td>
                                    <td type="R">0</td>
                                    <td type="H">0</td>
                                    <td type="RBI">0</td>
                                    <td type="BB">0</td>
                                    <td type="SO">0</td>
                                </tr>

                            @endforeach
                        </table>
                    </div>
                @endforeach
            </div>
            <div class="ui-lineup-div-title">
                <div class="js-button-lineup ui-button-lineup" selected="selected" team="visitor" type="pitchers">{{$game->teamVisitor->getSimpleName()}}</div>
                <div class="js-button-lineup ui-button-lineup" style="margin-left: -4px" team="home" type="pitchers">{{$game->teamHome->getSimpleName()}}</div>
            </div>
            <div class="js-container-lineup hidden">
                @foreach($lineupPlayers as $lineupTeam)
                    <div class="js-lineup ui-command-div hidden"  type="pitchers">
                        <div class="ui-caption-command">{{$lineupTeam['name']}}</div>
                        <table class="ui-table-command">
                            <tr>
                                <th colspan="2" class="ui-table-command-header">Pitchers</th>
                                <th>IP</th>
                                <th style="width: 81px;">PC</th>
                                <th>H</th>
                                <th>R</th>
                                <th>ER</th>
                                <th>BB</th>
                                <th>SO</th>
                            </tr>
                            @foreach ($lineupTeam['pitchers'] as $batter)
                                <tr class="js-lineup-pitcher" batter="{{$batter->BatterPosition}}" player="{{$batter->idbatter}}">
                                    <td>{{$batter->Number}}</td>
                                    <td class="ui-table-command-player">{{$batter->player->getCutName().' '.Batter::$defensePositions[$batter->DefensePosition]}}</td>
                                    <td type="IP">0</td>
                                    <td type="PC">0</td>
                                    <td type="H">0</td>
                                    <td type="R">0</td>
                                    <td type="ER">0</td>
                                    <td type="BB">0</td>
                                    <td type="SO">0</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endforeach
            </div>
            <div class="js-container-scorepad hidden">
                @foreach($lineupPlayers as $k=>$lineupTeam)
                    <div class="js-scorepad hidden" team="{{$teamType[$k]}}">
                        <div class="ui-caption-command">{{$lineupTeam['name']}}</div>
                        <div class="ui-scorepad-controls">
                            <div class="js-scorepad-button-prev"  style="display: none">&blacktriangleleft;</div>
                            <div class="js-scorepad-button-next" style="left: 330px;display: none">&blacktriangleright;</div>
                        </div>
                        <table class="ui-table-scorepad ui-table-command">
                            <tr class="js-scorepad-innings">
                                <th class="ui-table-command-header"></th>
                                <th style="width: 200px; text-align: left;">Player</th>
                                <th>P</th>
                                <th>I</th>
                            </tr>
                            @foreach($lineupTeam['lineup'] as $batter)
                                <tr class="js-scorepad-batter" batter="{{$batter->BatterPosition}}" player="{{$batter->idbatter}}">
                                    <td>{{$batter->Number}}</td>
                                    <td style="text-align: left">{{$batter->player->getFullName()}}</td>
                                    <td>{{$batter->getDefensePosition()}}</td>
                                    <td>{{$batter->Inning>>3}}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="ui-tables-right-div">
            <div class="ui-gamelog js-container-pitch-list hidden">
                <div class="js-pitch-list-controls">
                    <div class="js-left button" style="display: inline-block">&blacktriangleleft;</div>
                    <div class="js-counter pages" style="display: inline-block"></div>
                    <div class="js-right button" style="display: inline-block">&blacktriangleright;</div>
                    <div class="js-button-save button-save">Save</div>
                </div>
                <div class="js-container-scrollbar" style="height: 600px; width: 256px;">
                    <div class="js-pitch-list ui-pitch-status"></div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="js-scoreeditor ui-scoreeditor hidden">
        <div class="container">
            <h1></h1>
            <div class="button-close js-scoreeditor-button-close">&#9747;</div>
            <div class="tree-controls">
                <input class="js-scoreeditor-input-filter" type="text">
            </div>

            <div class="tree-container js-scoreeditor-tree">
                <div>
                </div>
            </div>
            <div class="tree-controls">
                <div class="button js-scoreeditor-button-add">Add</div>
                <div class="button js-scoreeditor-button-remove">Remove</div>
            </div>
            <div class="tree-add-container js-scoreeditor-tree-add">
                <div class="field">

                    <table>
                        <tr>
                            <td class="label">Score Category:</td>
                            <td class="label">
                                <select class="js-scoreeditor-select-type">
                                    <option value="none" selected="selected">Select Category</option>
                                    <option value="batters">Batter</option>
                                    <option value="fielders">Fielder</option>
                                    <option value="pitchers">Pitcher</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Score Type:</td>
                            <td class="label">
                                <select class="js-scoreeditor-select-score">
                                    <option value="none" selected="selected">Select Score</option>
                                </select>
                            </td>
                        </tr>
                    </table>

                </div>
                <div class="field">
                    <div class="button js-scoreeditor-button-save">Save</div>
                    <div class="button js-scoreeditor-button-cancel">Cancel</div>
                </div>
            </div>
        </div>
    </div>
@endif

@include('layout.menu',['atbat'=>true])
<div class="ui-footer">
    Copyright © {{date('Y')}} Northwoods League. All Rights Reserved.<br><br>
    <hr style="width:200px; margin:0 auto; height: 1px; background-color: white;">
</div>

@if(isset($lineupPlayers))
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/inning.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/pitching.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/field.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/hit.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/strike.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/out.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/misc.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/error.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/advance.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/graphics.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/request.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/storage.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/ballInPlay.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/gameLog.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/player.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/scorepad.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/score.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/scoreEditor.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/pitcherLimit.js')) !!}
{!! Html::script(LinkHelper::publicResource('/scripts/atbat/main.js')) !!}

<script>
    G.lineups = {
        visitor:{
            type: '{{$teamType[0]}}',
            name: '{{$lineupPlayers[0]['name']}}',
            batters: [
                @foreach ($lineupPlayers[0]['lineup'] as $batter)
                    {
                        id: {{$batter->idbatter}},
                        bats: '{{$batter->player->Bats}}',
                        number: {{$batter->Number}},
                        batter: {{$batter->BatterPosition}},
                        name: '{{$batter->player->getFullName()}}',
                        position: '{{Batter::$defensePositions[$batter->DefensePosition]}}',
                        lineupName: '{{$batter->player->getCutName().' '.Batter::$defensePositions[$batter->DefensePosition]}}',
                        inning: {{$batter->Inning>>3}},
                        inningOuts: {{$batter->Inning & 7}},
                        subOrder: {{$batter->SubOrder}}
                    },
                @endforeach
                ],
            fielders: [
                @foreach ($lineupPlayers[0]['fielders'] as $batter)
                    {
                        id: {{$batter->idbatter}},
                        number: {{$batter->Number}},
                        name: '{{$batter->player->getFullName()}}',
                        position: '{{Batter::$defensePositions[$batter->DefensePosition]}}',
                        positionId: {{$batter->DefensePosition}},
                        inning: {{$batter->Inning>>3}},
                        inningOuts: {{$batter->Inning & 7}},
                        subOrder: {{$batter->SubOrder}}
                    },
                @endforeach
                ],
            pitchers: [
                @foreach ($lineupPlayers[0]['pitchers'] as $batter)
                    {
                        id: {{$batter->idbatter}},
                        throws: '{{$batter->player->Throws}}',
                        number: {{$batter->Number}},
                        batter: {{$batter->BatterPosition}},
                        name: '{{$batter->player->getFullName()}}',
                        position: '{{Batter::$defensePositions[$batter->DefensePosition]}}',
                        lineupName: '{{$batter->player->getCutName().' '.Batter::$defensePositions[$batter->DefensePosition]}}',
                        inning: {{$batter->Inning>>3}},
                        inningOuts: {{$batter->Inning & 7}},
                        subOrder: {{$batter->SubOrder}}
                    },
                @endforeach
            ]

            },
        home:{
            type: '{{$teamType[1]}}',
            name: '{{$lineupPlayers[1]['name']}}',
            batters: [
                @foreach ($lineupPlayers[1]['lineup'] as $batter)
                {
                    id: {{$batter->idbatter}},
                    bats: '{{$batter->player->Bats}}',
                    number: {{$batter->Number}},
                    batter: {{$batter->BatterPosition}},
                    name: '{{$batter->player->getFullName()}}',
                    position: '{{Batter::$defensePositions[$batter->DefensePosition]}}',
                    lineupName: '{{$batter->player->getCutName().' '.Batter::$defensePositions[$batter->DefensePosition]}}',
                    inning: {{$batter->Inning>>3}},
                    inningOuts: {{$batter->Inning & 7}},
                    subOrder: {{$batter->SubOrder}}
                },
                @endforeach
                ],
            fielders: [
                @foreach ($lineupPlayers[1]['fielders'] as $batter)
                    {
                        id: {{$batter->idbatter}},
                        number: {{$batter->Number}},
                        name: '{{$batter->player->getFullName()}}',
                        position: '{{Batter::$defensePositions[$batter->DefensePosition]}}',
                        positionId: {{$batter->DefensePosition}},
                        inning: {{$batter->Inning>>3}},
                        inningOuts: {{$batter->Inning & 7}},
                        subOrder: {{$batter->SubOrder}}
                    },
                @endforeach
                ],
            pitchers: [
                @foreach ($lineupPlayers[1]['pitchers'] as $batter)
                    {
                        id: {{$batter->idbatter}},
                        throws: '{{$batter->player->Throws}}',
                        number: {{$batter->Number}},
                        batter: {{$batter->BatterPosition}},
                        name: '{{$batter->player->getFullName()}}',
                        position: '{{Batter::$defensePositions[$batter->DefensePosition]}}',
                        lineupName: '{{$batter->player->getCutName()}}',
                        inning: {{$batter->Inning>>3}},
                        inningOuts: {{$batter->Inning & 7}},
                        subOrder: {{$batter->SubOrder}}
                    },
                @endforeach
            ]
        }
    };

    G.isPitchTrackingEnabled = {{$settings['usePitchTracker']? 'true': 'false'}};
    G.pitcherPCLimits = {
        innPCLimit: {{$settings['innPCLimit']}},
        totalPCLimit: {{$settings['totalPCLimit']}}
    };
    G.regularGameInning = {{$settings['regGame']}};
    G.extraGameInning = 9;

</script>
@endif
<script>
    G = window.G || {};
    G.gameId = {{$game->idgame}};
    G.token = '{{csrf_token()}}';
    G.baseUrl = '{{URL::to('/')}}';
    G.gameStatus = {{$game->status}};

    $.contextMenu( 'destroy', '.js-button-misc-context' );
    $.contextMenu({
        selector: '.js-button-misc-context ',
        trigger: 'left',
        items:{
            end:{
                name: 'End Game',
                items:{
                    endRegulation:{
                        name: 'Regulation',
                        callback: function(){
                            G.onUpdateGameStatus(2);
                        }
                    },
                    endExtraInnings:{
                        name: 'Extra Innings',
                        callback: function(){
                            G.onUpdateGameStatus(3);
                        }
                    },
                    endTimeLimit:{
                        name: 'Rain Counts',
                        callback: function(){
                            G.onUpdateGameStatus(4);
                        }
                    }
                }
            },
            suspend:{
                name: 'Suspend Game',
                items:{
                    suspendRain:{
                        name: 'Rain',
                        callback: function(){
                            G.onUpdateGameStatus(5);
                        }
                    },
                    suspendOther:{
                        name: 'Other',
                        callback: function(){
                            G.onUpdateGameStatus(6);
                        }
                    }
                }
            },
            delayed:{
                name: 'Delayed Game',
                items:{
                    delayedRain:{
                        name: 'Rain',
                        callback: function(){
                            G.onUpdateGameStatus(7);
                        }
                    },
                    delayedOtherWether:{
                        name: 'Other Weather',
                        callback: function(){
                            G.onUpdateGameStatus(8);
                        }
                    },
                    delayedOther:{
                        name: 'Other',
                        callback: function(){
                            G.onUpdateGameStatus(9);
                        }
                    }
                }
            },
            {{--close:{--}}
                {{--name: 'Close',--}}
                {{--callback: function(){--}}
                    {{--G.onRedirect('{{action('GameController@index')}}');--}}
                {{--}--}}
            {{--},--}}

//            debug:{
//                name: 'Debug',
//                items:{
//                    getJson: {
//                        name: 'Get Storage Json',
//                        callback: function(item){
//                            G.onDebug(item);
//                        }
//                    },
//                    restore: {
//                        name: 'Restore',
//                        callback: function(item){
//                            G.onDebug(item);
//                        }
//                    },
//                    undo: {
//                        name: 'Undo',
//                        callback: function(item){
//                            G.onDebug(item);
//                        }
//                    }
//                }
//            }
        }
    });

    // If At Bat has errors
    G.onRedirect = G.onRedirect || function(url){location = url;};

    $('.ui-menu-element a').click(function(){
        G.onRedirect($(this).attr('href'));
        return false;
    });

</script>
</body>
</html>