@extends('game.update')

@section('header-part')
    {!! Html::style(LinkHelper::publicResource('/css/update/statistics.css')) !!}
    @yield('header-sub-part')
@stop

@section('content-form')
    <div>
        @yield('content-statistics')
    </div>
@stop