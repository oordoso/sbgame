/**
 * @param {string} text
 * @param {Function} callback
 */
function warning(text, callback) {
    'use strict';

    $('.js-warning-shadow').remove();
    var $warning = $('<div class="ui-warning-shadow js-warning-shadow">')
        .append($('<div class="ui-warning-container">')
            .append($('<div  class="ui-warning-header">').html('<b>Warning</b>'))
            .append($('<div>').text(text))
            .append($('<div class="ui-warning-controls">')
                .append($('<div class="ui-warning-button">')
                    .text('OK')
                    .click(function () {
                        $warning.remove();
                        if (callback) {
                            callback(text);
                        }
                    }))));

    $('body').append($warning);
}

