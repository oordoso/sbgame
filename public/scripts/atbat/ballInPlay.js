function BallInPlayController() {
    'use strict';

    var self = this;
    var menu = null;
    var enableNextBatter = true;

    var types = {
        //'HBP': 'hit',
        '1B': 'hit',
        '2B': 'hit',
        '3B': 'hit',
        'HR': 'hit',

        "flyOuts": {
            'F': 'out',
            'LO': 'out',
            'PO': 'out',
            'FBO': 'out',
            'IFR': 'out'
        },
        'GO': 'out',
        'SF': 'out',
        'SH': 'out',
        'FO': 'out',
        'TO': 'out',
        'DP': 'out',
        'TP': 'out',
        //'PO': 'out',

        //'KC': 'strike',
        //'KS': 'strike',

        //'ERR': 'error',

        //'SB': 'misc',
        //'CS': 'misc',
        //'WP': 'misc',

        'AR': 'advance'
    };

    function initMenu() {
        function getName(type) {
            switch (type) {
                case 'flyOuts':
                    return 'Fly Outs';
                case 'F':
                    return 'Fly Out';
                case 'LO':
                    return 'Line Out';
                case 'PO':
                    return 'Pop Out';
                case 'FBO':
                    return 'Foul Ball Out ';
                case 'IFR':
                    return 'Infield Fly Rule';
                case 'GO':
                    return 'Ground Out';
                case 'SF':
                    return 'Sac Fly';
                case 'SH':
                    return 'Sac Bunt';
                case 'FO':
                    return 'Force Out';
                case 'TO':
                    return 'Tag Out';
                case 'DP':
                    return 'Double Play';
                case 'TP':
                    return 'Triple Play';
                case 'PK':
                    return 'Picked Off Base';
                case 'ERR':
                    return 'Error';
                case 'SB':
                    return 'Stolen Base';
                case 'CS':
                    return 'Caught Stealing';
                case 'AR':
                    return 'Advanced Run';
            }

            return type;
        }

        function bindItems(types, obj) {
            for (var t in types) {
                if (typeof types[t] === 'object') {
                    obj[t] = {
                        name: getName(t),
                        items: {}
                    };
                    bindItems(types[t], obj[t].items);
                }
                if (typeof types[t] === 'string') {
                    obj[t] = {
                        name: getName(t),
                        callback: menuHandle,
                        disabled: disableHandle
                    };
                }
            }
        }

        var items = {};
        bindItems(types, items);

        menu = $.contextMenu({
            selector: '.js-button-pitch-menu',
            trigger: 'left',
            items: items
        });
    }

    function menuHandle(item) {
        if (!self.onMenu(types[item] || types.flyOuts[item], item)) {
            window.warning('This option is disabled');
        }
    }

    function disableHandle(item) {
        return !self.onIsEnabled(types[item] || types.flyOuts[item], item);
    }

    self.onMenu = function (type, item) {
    };
    self.onIsEnabled = function (type, item) {
    };

    self.disableNextBatter = function () {
        enableNextBatter = false;
    };

    self.enableFull = function () {
        enableNextBatter = true;
    };

    initMenu();
}