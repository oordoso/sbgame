window.G = {};

$('body').ready(function () {
    'use strict';

    var $lineups = $('.js-lineup');
    var $scorepads = $('.js-scorepad');

    $lineups = {
        batters: {
            visitor: $lineups.eq(0),
            home: $lineups.eq(1)
        },
        pitchers: {
            visitor: $lineups.eq(2),
            home: $lineups.eq(3)
        }
    };

    $scorepads.visitor = $scorepads.eq(0);
    $scorepads.home = $scorepads.eq(1);

    /*global PitchingController */
    /*global InningController */
    /*global FieldController */
    /*global HitController */
    /*global GraphicsController */
    /*global StrikeController */
    /*global OutController */
    /*global MiscController */
    /*global ErrorController */
    /*global AdvanceController */
    /*global RequestController */
    /*global StorageController */
    /*global BallInPlayController */
    /*global GameLogController */
    /*global PlayerController */
    /*global ScorePadController */
    /*global ScoreController */
    /*global ScoreEditorController */
    /*global PitcherLimitController */

    var pitcherController = new PitchingController();
    var inningController = new InningController();
    var fielderController = new FieldController();
    var hitController = new HitController();
    var graphicsController = new GraphicsController();
    var strikeController = new StrikeController();
    var outController = new OutController();
    var miscController = new MiscController();
    var errorController = new ErrorController();
    var advanceController = new AdvanceController();
    var requestController = new RequestController();
    var storageController = new StorageController();
    var ballInPlayController = new BallInPlayController();
    var gameLogController = new GameLogController();
    var playerController = new PlayerController();
    var scorePadController = new ScorePadController();
    var scoreController = new ScoreController();
    var scoreEditorController = new ScoreEditorController();
    var pitcherLimitController = new PitcherLimitController();

    requestController.register(storageController);
    requestController.register(pitcherLimitController);

    storageController.register(pitcherController);
    storageController.register(inningController);
    storageController.register(fielderController);
    storageController.register(hitController);
    storageController.register(graphicsController);
    storageController.register(strikeController);
    storageController.register(outController);
    storageController.register(miscController);
    storageController.register(gameLogController);
    storageController.register(playerController);
    storageController.register(scorePadController);
    storageController.register(scoreEditorController);
    storageController.register(pitcherLimitController);

    playerController.register(inningController);
    playerController.register(fielderController);
    playerController.register(gameLogController);
    playerController.register(scoreController);
    playerController.register(scoreEditorController);
    playerController.register(pitcherLimitController);

    inningController.register(scoreController);


    var sleepData = {
        callbackSequence: [],
        lastHandle: -1,
        isSleep: false
    };

    var gameActionStarted = false;

    function registerLogging(obj) {
        function getDecorator(o, name, func) {
            return function () {
                window.console.log(o.constructor.name, name, [].slice.call(arguments));
                return func.apply(o, [].slice.call(arguments));
            };
        }

        for (var pr in obj) {
            if (obj.hasOwnProperty(pr) && typeof obj[pr] == "function") {
                obj[pr] = getDecorator(obj, pr, obj[pr]);
            }
        }
    }

    function log() {
        window.console.log(arguments);
    }

    function initLineupSwitching() {
        $('.js-button-lineup').click(function () {
            var $this = $(this);
            var type = $this.attr('type');
            $('.js-button-lineup[type=' + type + ']').removeAttr('selected');
            $lineups[type].visitor.hide();
            $lineups[type].home.hide();

            $this.attr('selected', 1);
            $lineups[type][$this.attr('team')].show();

            if (type == 'batters') {
                $scorepads.hide();
                $scorepads[$this.attr('team')].show();
            }
        });

        $('.js-button-lineup[team=visitor][type=batters]').click();
        $('.js-button-lineup[team=visitor][type=pitchers]').click();
    }

    function initContainerSwitching() {
        $('.js-button-container').click(function () {
            $('.js-button-container').removeAttr('selected');
            $('.js-container-lineup').hide();
            $('.js-container-scorepad').hide();

            $(this).attr('selected', 1);
            $('.js-container-' + $(this).attr('type')).show();

            if ($(this).attr('type') == 'lineup') {
                $('.js-button-lineup[type=pitchers]').parent().show();
            } else {
                $('.js-button-lineup[type=pitchers]').parent().hide();
            }
        });

        $('.js-button-container[type=lineup]').click();
    }

    function initScrollBar() {
        $('.js-container-scrollbar').height($('.js-table-left-div').height() - 52);
    }

    function initNextButton() {
        $('.js-button-game-next').click(function () {
            nextBatter();
            $('.js-button-game-next').hide();
            $('.js-button-game-undo').show();
        });
    }

    function initUndoButton() {
        $('.js-button-game-undo').click(function () {
            storageController.undoByPitchSafe();
            restore();
            graphicsController.clear();
        });
    }

    function initGameActionButton() {
        var status = window.G.gameStatus;

        if (status === 0) {
            updateGameActionButton('start');
            return;
        }

        if (status === 1) {
            updateGameActionButton('stop');
            return;
        }

        if (status >= 2 && status <= 4) {
            updateGameActionButton('info');
            return;
        }

        if (status >= 5 && status <= 9) {
            updateGameActionButton('resume');
        }
    }

    function updateGameActionButton(state) {
        var $button = $('.js-button-game-action');
        var $label = $button.children('.text');
        var $infoRef = $button.children('a');
        switch (state) {
            case 'start':
                $label.text('START GAME');
                $button.off().on('click', function () {
                    window.G.onUpdateGameStatus(1);
                });
                break;
            case 'resume':
                $label.text('RESUME GAME');
                $button.off().on('click', function () {
                    window.G.onUpdateGameStatus(1);
                });
                break;
            case 'stop':
                $label.text('STOP GAME');
                $button.off().on('mousedown', function (e) {
                    $('.js-button-misc-context').contextMenu({x: e.pageX, y: e.pageY});
                });
                break;
            case 'info':
                $label.text('GAME INFO');
                $button.off().on('click', function () {
                    window.location = $infoRef.attr('href');
                });
                break;
        }
    }

    function selectLineup(type) {
        if (type == 'home') {
            $('.js-button-lineup[team=home][type=batters]').click();
            $('.js-button-lineup[team=visitor][type=pitchers]').click();
        } else {
            $('.js-button-lineup[team=visitor][type=batters]').click();
            $('.js-button-lineup[team=home][type=pitchers]').click();
        }
    }

    //function sleep(callback) {
    //    if (sleepData.lastHandle == -1) {
    //        sleepData.lastHandle = setTimeout(function () {
    //            for (var i in sleepData.callbackSequence) {
    //                sleepData.callbackSequence[i]();
    //            }
    //            sleepData.callbackSequence = [];
    //            sleepData.lastHandle = -1;
    //        }, 2000);
    //    }
    //
    //    sleepData.callbackSequence.push(callback);
    //}

    function nextBatter() {
        miscController.enable(false);
        advanceController.enable(false);

        fielderController.clearMarks();
        inningController.nextBatter();

        inningController.updateLineup();
        var gameSet = inningController.getGameSet();
        fielderController.setGameSet(gameSet);
        fielderController.updateLineup();

        scorePadController.update();

        // TODO: Fix this
        storageController.storeState();
    }

    function tryDoNextBatter() {
        advanceController.enable(true);
        $('.js-button-game-next').show();
        $('.js-button-game-undo').hide();
    }

    function restore() {
        inningController.restore();

        var gameSet = inningController.getGameSet();

        pitcherController.restore();
        fielderController.restore(gameSet);
        gameLogController.restore();

        scorePadController.restore();
        pitcherLimitController.restore();

        selectLineup(gameSet.offenceLineup.type);
    }

    function tryRestoreFromServer() {
        if (window.G.gameStatus == 1) {
            storageController.restoreState(function () {
                restore();
            });
        }
    }

    function enableGeneralControllers(isEnable) {
        hitController.enable(isEnable);
        outController.enable(isEnable);
        errorController.enable(isEnable);
    }

    function reload() {
        window.location = window.location;
    }

    //window.G.storage = storageController;

    pitcherController.on3Strikes = function (type) {
        // strikeController used only for strike out
        strikeController.enable(true);
        strikeController.menuHandle(type);
    };

    pitcherController.on4Balls = function () {
        scoreController.on4Balls();
        fielderController.doAutoBatterBase();
        graphicsController.drawLabel('BB');
        enableGeneralControllers(false);
        tryDoNextBatter();
    };

    // This method should be without any actions (like nextBatter)
    pitcherController.on3Outs = function () {
        inningController.set3Outs();
    };

    pitcherController.onBeforePitch = function () {
        hitController.enableParticular(true);
        miscController.enable(true);
    };

    pitcherController.onPitch = function (type) {
        inningController.updatePitchActors();
        scoreController.onPitch(type);
        pitcherLimitController.onPitch();
        gameLogController.addPitchCount();
        gameLogController.update();
    };

    pitcherController.onEnable = function (isEnable) {
        enableGeneralControllers(isEnable);
    };

    pitcherController.onAddLog = function (type, text) {
        gameLogController.addItem(type, text);
        gameLogController.update();
    };

    pitcherController.onWildPitch = function () {
        scoreController.onPitchError('WP');
        graphicsController.drawStateLabel('WP');
        advanceController.enable(true);
    };

    pitcherController.onBeforeHitByPitch = function(){
        scoreController.onPitchError('HBP');
        fielderController.doAutoBatterBase();
    };

    pitcherController.onHitByPitch = function () {
        pitcherController.enable(false);
        enableGeneralControllers(false);
        graphicsController.drawLabel('HBP');
        tryDoNextBatter();
    };

    inningController.onBatterReady = function (batter) {
        pitcherController.resetPitching();
        fielderController.setCurrentBatter(batter);
        scoreController.onBatterReady();
        graphicsController.clear();
    };

    inningController.onBeforeNextGameSet = function () {
        scoreController.onBeforeNextGameSet();
    };

    inningController.onNextGameSet = function () {
        var gameSet = inningController.getGameSet();
        fielderController.setGameSet(gameSet);
        fielderController.resetFielders();
        pitcherController.reset();
        pitcherLimitController.resetInning();
        selectLineup(gameSet.offenceLineup.type);
    };

    inningController.onGameEnd = function (status, winner) {
        pitcherController.reset();
        pitcherController.enable(false);
        fielderController.clear();
        graphicsController.clear();

        enableGeneralControllers(false);

        requestController.setGameStatus(status, function (data) {
            if (data.success) {
                storageController.storeState(function () {
                    if (!winner) {
                        window.warning('The winner is undefined', reload);
                    } else {
                        if (winner == 'deadheat') {
                            window.warning('Game is finished with dead heat', reload);
                        } else {
                            window.warning('The ' + winner + ' team wins the game', reload);
                        }
                    }
                });

            }
        });
    };

    inningController.onGameSuspend = function (status, uiStatus) {
        storageController.storeState(function () {
            requestController.setGameStatus(status, function (data) {
                if (data.success) {
                    storageController.storeState(function () {
                        window.warning('Game is ' + uiStatus, reload);
                    });
                }
            });
        });
    };

    fielderController.onAddInningScore = function (batter) {
        scoreController.onAddInningScore(batter);
        inningController.addInningRunScore();
    };

    fielderController.onBatterRun = function (base) {
        graphicsController.drawRun(base);
    };

    fielderController.onBatterFielderClick = function (obj) {
        outController.doClick(obj);
        miscController.doClick(obj);
        errorController.doClick(obj);
        advanceController.doClick(obj);
    };

    hitController.onBatterHit = function (type) {
        fielderController.doBatterHit(type);
        scoreController.onBatterHit(type);
        inningController.addInningHitScore();
        pitcherController.addHitPitch(type);
        pitcherController.enable(false);
        enableGeneralControllers(false);
        tryDoNextBatter();
    };

    hitController.onPitchError = function (type) {
        scoreController.onPitchError(type);

        if (type == 'WP' || type == 'PB') {
            pitcherController.addFoulPitch(type);
            graphicsController.drawStateLabel(type);
            advanceController.enable(true);
            return;
        }

        pitcherController.addFoulPitch(type);
        // TODO: Incorrect behavior: addHitPitch should call before doAutoBatterBase (onUpdatePitch)

        if (type == 'BK') {
            fielderController.doBatterBaseForce(3, 0);
        } else {
            fielderController.doAutoBatterBase();
        }

        //inningController.addInningErrorScore();
        pitcherController.enable(false);
        enableGeneralControllers(false);
        graphicsController.drawLabel(type);
        tryDoNextBatter();
    };

    hitController.onDrawHit = function (point) {
        graphicsController.clear();
        graphicsController.drawHit(point);
    };

    hitController.onDrawLabel = function (type) {
        graphicsController.drawLabel(type);
    };

    strikeController.onStrikeOut = function (type) {
        scoreController.onStrikeOut(type);
        graphicsController.drawStateLabel(type == 'KC' ? '<span class="ui-k">K</span>' : 'K');
        fielderController.doBatterOut();
        pitcherController.addOut();
        pitcherController.addOutPitch(type);
        pitcherController.enable(false);
        strikeController.enable(false);
        enableGeneralControllers(false);
        tryDoNextBatter();
    };

    outController.onOut = function (type, fielders, batters) {
        pitcherController.addOutPitch(type);
        hitController.enable(false);

        scoreController.onOut(type, fielders, batters);

        if (batters.length > 0) {
            var fieldersLabel = fielders.length > 1 ? ' ' + fielders.join('-') : fielders;
            graphicsController.drawStateLabel(type + fieldersLabel);
        }

        if (batters.length === 0 ||
            type != 'F' &&
            type != 'LO' &&
            type != 'PO' &&
            type != 'FBO' &&
            type != 'IFR' &&
            type != 'SF') {
            fielderController.markBatterBase();
        }

        fielderController.doBattersOut(batters);

        for (var k in batters) {
            pitcherController.addOut();
        }

        if (pitcherController.is3Outs()) {
            fielderController.updateBatters();
            fielderController.storeBaseState([]);
        } else {
            fielderController.doBatterBase();
        }


        pitcherController.enable(false);
        enableGeneralControllers(false);
        tryDoNextBatter();
    };

    outController.onDrawHit = function (point) {
        graphicsController.clear();
        graphicsController.drawHit(point);
    };

    outController.onBatterBase = function (toBase) {
        toBase = toBase || 1;
        fielderController.doBatterBaseForce(3, toBase);
    };

    miscController.onOut = function (batter) {
        pitcherController.addOut();
        scoreController.onMiscOut(batter.id);
        graphicsController.drawStateLabel('CS' + ' ' + (batter.position < 3 ? (batter.position + 1) + 'B' : 'Home'));
        fielderController.doBattersOut([batter.position]);
        fielderController.updateBatters();
        fielderController.storeBaseState([]);
        gameLogController.addItem('out', 'CS');
        gameLogController.update();
        inningController.updatePitchActors();
        pitcherController.passCurrentPitch();

        if (pitcherController.is3Outs()) {
            pitcherController.enable(false);
            enableGeneralControllers(false);
            miscController.enable(false);
            tryDoNextBatter();
            advanceController.enable(false);
        }
    };

    miscController.onBatterBase = function (batter) {
        scoreController.onMiscBatterBase(batter.id);
        fielderController.doBatterBaseForce(batter.position, batter.position);
        graphicsController.drawStateLabel('SB' + ' ' + (batter.position < 3 ? (batter.position + 1) + 'B' : 'Home'));
        gameLogController.addItem('misc', 'SB');
        gameLogController.update();
        inningController.updatePitchActors();
        pitcherController.passCurrentPitch();
    };

    errorController.onError = function (obj) {
        inningController.addInningErrorScore();
        scoreController.onError(obj);
    };

    advanceController.onAdvancing = function (from, to) {
        fielderController.doAutoBatterBase(to, from);
    };

    storageController.onUpdatePitch = function () {
        storageController.onUpdateState();
        pitcherController.tryCreatePitch();
    };

    storageController.onUpdateState = function () {
        inningController.tryCreateState();
    };

    ballInPlayController.onMenu = function (type, item) {
        switch (type) {
            case 'hit':
                return hitController.menuHandle(item);
            case 'out':
                return outController.menuHandle(item);
            case 'strike':
                return strikeController.menuHandle(item);
            case 'error':
                return errorController.menuHandle(item);
            case 'misc':
                return miscController.menuHandle(item);
            case 'advance':
                return advanceController.menuHandle(item);
        }
        return false;
    };

    ballInPlayController.onIsEnabled = function (type, item) {
        switch (type) {
            case 'hit':
                return hitController.enable();
            case 'out':
                return outController.enable();
            case 'strike':
                return strikeController.enable();
            case 'error':
                return errorController.enable();
            case 'misc':
                return miscController.enable();
            case 'advance':
                return advanceController.enable();
        }
        return false;
    };

    gameLogController.onGetCurrentTeamBatter = function () {
        var gameSet = inningController.getGameSet();
        var batter = inningController.getCurrentBatter();

        return {
            lineup: gameSet.offenceLineup.name,
            batter: batter.name
        };
    };

    scoreEditorController.onEdited = function () {
        inningController.updateScores();
    };

    //registerLogging(pitcherController);
    //registerLogging(inningController);
    //registerLogging(fielderController);
    //registerLogging(hitController);
    //registerLogging(graphicsController);
    //registerLogging(strikeController);
    //registerLogging(outController);
    //registerLogging(miscController);
    //registerLogging(errorController);
    //registerLogging(advanceController);
    //registerLogging(requestController);
    //registerLogging(storageController);
    //registerLogging(ballInPlayController);
    //registerLogging(gameLogController);
    //registerLogging(playerController);
    //registerLogging(scorePadController);
    //registerLogging(scoreController);
    //registerLogging(scoreEditorController);
    //registerLogging(pitcherLimitController);

    window.G.onUpdateGameStatus = function _f(gamestatus) {
        if (gameActionStarted) {
            window.warning('Please wait. The action is not complete.');
            return;
        }

        gameActionStarted = true;

        switch (gamestatus) {
            case 1:
                requestController.getGameStatus(function (data) {
                    if (data.status === 1) {
                        window.warning('Game is in progress');
                        gameActionStarted = false;
                        return;
                    }

                    if (data.status >= 2 && (data.status <= 4)) {
                        window.warning('Game is over. You can not start finished game.');
                        gameActionStarted = false;
                        return;
                    }

                    if (data.status === 0) {
                        requestController.setGameStatus(gamestatus, function (data) {
                            if (data.success) {
                                window.G.gameStatus = gamestatus;
                                inningController.startGame();
                                scorePadController.restore();
                                storageController.storeState(function () {
                                    updateGameActionButton('stop');
                                    gameActionStarted = false;
                                });
                            }
                        });
                    }

                    if (data.status >= 5 && data.status <= 9) {
                        requestController.setGameStatus(gamestatus, function (data) {
                            if (data.success) {
                                window.G.gameStatus = gamestatus;
                                storageController.restoreState(function () {
                                    restore();
                                    updateGameActionButton('stop');
                                    gameActionStarted = false;
                                });
                            }
                        });
                    }
                });
                break;
            default:
                requestController.getGameStatus(function (data) {
                    if (data.status === 0) {
                        window.warning('Game is not started. You can not finish the game before start.');
                        gameActionStarted = false;
                        return;
                    }

                    if (data.status >= 2 && (data.status <= 4)) {
                        window.warning('Game is already over.');
                        gameActionStarted = false;
                        return;
                    }

                    if (data.status >= 5 && data.status <= 6) {
                        window.warning('Game is suspended. You can not finish the game before resume.');
                        gameActionStarted = false;
                        return;
                    }

                    if (data.status >= 7 && data.status <= 9) {
                        window.warning('Game is delayed. You can not finish the game before resume.');
                        gameActionStarted = false;
                        return;
                    }

                    if (data.status === 1) {
                        gameActionStarted = inningController.endGame(gamestatus) !== false;
                    }
                });
                break;
        }
    };

    window.G.onDebug = function (item) {
        if (item == 'getJson') {
            window.console.log(JSON.stringify(storageController, null, '\t'));
        }

        if (item == 'restore') {
            var obj = JSON.parse('{"currentInning":0,"currentState":3,"currentPitch":0,"innings":[{"state":[{"pitches":[{"coordinatesCatch":{"x":98.09375,"y":33},"number":1,"type":"hit","type2":"2B","coordinatesHit":{"x":246.09375,"y":40}}],"playerScores":[{"id":3485,"type":"batters","score":"AB"},{"id":3505,"type":"pitchers","score":"IP"},{"id":3485,"type":"batters","score":"H"},{"id":3505,"type":"pitchers","score":"H"}],"inningScores":[{"type":"visitor","score":"H"}],"offence":"visitor","defence":"home","pitcher":3505,"outs":0,"batter":1},{"pitches":[{"coordinatesCatch":{"x":82.09375,"y":79},"number":1,"type":"ball","counter":{"total":1,"strike":0,"ball":1,"out":0}},{"coordinatesCatch":{"x":106.09375,"y":51},"number":2,"type":"ball","counter":{"total":2,"strike":0,"ball":2,"out":0}},{"coordinatesCatch":{"x":46.09375,"y":44},"number":3,"type":"ball","counter":{"total":3,"strike":0,"ball":3,"out":0}},{"coordinatesCatch":{"x":89.09375,"y":98},"number":4,"type":"ball","counter":{"total":4,"strike":0,"ball":4,"out":0}}],"playerScores":[{"id":3486,"type":"batters","score":"AB"},{"id":3505,"type":"pitchers","score":"IP"},{"id":3505,"type":"pitchers","score":"IP"},{"id":3505,"type":"pitchers","score":"IP"},{"id":3505,"type":"pitchers","score":"IP"},{"id":3486,"type":"batters","score":"BB"},{"id":3505,"type":"pitchers","score":"BB"}],"inningScores":[],"offence":"visitor","defence":"home","pitcher":3505,"outs":0,"batter":2},{"pitches":[{"coordinatesCatch":{"x":103.09375,"y":53},"number":1,"type":"strike","counter":{"total":1,"strike":1,"ball":0,"out":1},"type2":"swinging"}],"playerScores":[{"id":3487,"type":"batters","score":"AB"},{"id":3505,"type":"pitchers","score":"IP"},{"id":3487,"type":"batters","score":"SO"},{"id":3505,"type":"pitchers","score":"SO"}],"inningScores":[],"offence":"visitor","defence":"home","pitcher":3505,"outs":1,"batter":3},{"pitches":[{"coordinatesCatch":{"x":87.09375,"y":63},"number":1,"type":"ball","counter":{"total":1,"strike":0,"ball":1,"out":1}}],"playerScores":[{"id":3488,"type":"batters","score":"AB"},{"id":3505,"type":"pitchers","score":"IP"}],"inningScores":[],"offence":"visitor","defence":"home","pitcher":3505,"outs":1,"batter":4}],"bases":[{"id":-1,"bases":[],"runs":[]},{"id":0,"bases":[null,null,3485],"runs":[0,1]},{"id":13,"bases":[null,3486,3485],"runs":[0]},{"id":20,"bases":[null,3486,3485],"runs":[]}]}]}');
            storageController.setData(obj);

            inningController.restore();

            var gameSet = inningController.getGameSet();

            pitcherController.reset();
            pitcherController.restore();
            fielderController.setGameSet(gameSet);
            fielderController.restore();
            gameLogController.restore();

            scorePadController.restore();
            pitcherLimitController.restore();

            selectLineup(gameSet.offenceLineup.type);
        }

        if (item == 'undo') {
            storageController.undoBy('pitch');
            restore();
        }
    };

    window.G.onRedirect = function (url) {
        if (window.G.gameStatus && window.G.gameStatus == 1) { // game in progress
            if (sleepData.lastHandle == -1) { // not wait next
                storageController.storeState(function () {
                    window.location = url;
                });
            }
        } else {
            window.location = url;
        }
    };

    initContainerSwitching();
    initLineupSwitching();
    initUndoButton();
    initScrollBar();
    initNextButton();
    initGameActionButton();

    tryRestoreFromServer();

});