function PlayerController() {
    'use strict';

    var self = this;

    var runners;

    resetGlobals();

    function resetGlobals() {
        runners = {};
    }

    /** @type StorageController */
    self.storage = null;


    self.getInningPlayers = function (team, type, inning) {
        function getFilterBatterOffence(player) {
            return function (b) {
                return b.subOrder > 0 &&
                    b.inning <= inning &&
                    b.batter == player.batter &&
                    (   b.position == 'PH' ||
                    bases.indexOf(b.batter) != -1 && b.position == 'PR' ||
                    b.position != 'PH' && b.position != 'PR') &&
                    (   b.position == 'P' && (
                    b.inningOuts > 0 && team == 'home' ||
                    b.inning < inning) ||
                    b.position != 'P');
            };
        }

        function getFilterBatterDefense(player) {
            return function (b) {
                return b.subOrder > 0 &&
                    b.inning <= inning &&
                    b.batter == player.batter &&
                    b.position != 'PH' && b.position != 'PR';
            };
        }

        function getFilterOthersOffence(player) {
            return function (b) {
                return b.subOrder > 0 && b.inning <= inning && b.position == player.position;
            };
        }

        function getFilterOthersDefense(player) {
            return function (b) {
                return b.subOrder > 0 &&
                    b.inning <= inning && b.position == player.position &&
                    (   b.position == 'P' && (
                    b.inningOuts <= outs ||
                    b.inning < inning) ||
                    b.position != 'P');
            };
        }


        if (!window.G.lineups[team] || !window.G.lineups[team][type] || inning < 0) {
            return null;
        }

        // state can have undefined params!!
        var state = self.storage.getState();
        // get params with defaults
        var bases = (self.storage.getBaseState() || {}).bases || [];
        var outs = state.outs || 0;
        var defence = state.defence || "home";

        var isDefense = defence == team;
        var isOffence = !isDefense;

        var res = [];
        var k;
        for (k in window.G.lineups[team][type]) {
            if (window.G.lineups[team][type][k].subOrder === 0) {
                res.push(window.G.lineups[team][type][k]);
            }
        }

        for (k in res) {
            var player = res[k];
            var substitutions = [];

            // TODO: Rewrite substitutions
            if (type == 'batters') {
                if (isOffence) {
                    substitutions = window._.filter(window.G.lineups[team][type], getFilterBatterOffence(player));
                }
                if (isDefense) {
                    substitutions = window._.filter(window.G.lineups[team][type], getFilterBatterDefense(player));
                }
            } else {

                if (isOffence) {
                    substitutions = window._.filter(window.G.lineups[team][type], getFilterOthersOffence(player));
                }

                if (isDefense) {
                    substitutions = window._.filter(window.G.lineups[team][type], getFilterOthersDefense(player));
                }
            }

            res[k] = window._.last(substitutions) || res[k];
        }

        return res;
    };

    self.getPlayers = function (team, type) {
        if (!window.G.lineups[team] || !window.G.lineups[team][type]) {
            return null;
        }

        var res = [];
        var k = 0;
        for (k in window.G.lineups[team][type]) {
            res.push(window.G.lineups[team][type][k]);
        }

        return res;
    };

    self.getInningLineup = function (team, inning) {
        return {
            type: window.G.lineups[team].type,
            name: window.G.lineups[team].name,
            batters: self.getInningPlayers(team, 'batters', inning),
            fielders: self.getInningPlayers(team, 'fielders', inning),
            pitchers: self.getInningPlayers(team, 'pitchers', inning),
            getPlayer: function (id, type) {
                return self.getPlayer(id, type);
            }
        };
    };

    self.getPlayer = function (id, type) {
        return window._.find(window.G.lineups.visitor[type], function (b) {
                return b.id == id;
            }) ||
            window._.find(window.G.lineups.home[type], function (b) {
                return b.id == id;
            });
    };

    self.getCurrentFielder = function (positionId) {
        // state can have undefined params!!
        var state = self.storage.getState();
        // get params with defaults
        var defence = state.defence || 'home';

        return self.getInningPlayers(defence, 'fielders', self.storage.currentInning + 1).filter(function (p) {
            return p.positionId == positionId;
        })[0];
    };

    self.getCurrentPlayers = function (type) {
        // state can have undefined params!!
        var state = self.storage.getState();
        // get params with defaults
        var defence = state.defence || 'home';
        var offence = state.offence || 'visitor';

        if (type == 'batters') {
            return self.getInningPlayers(offence, type, self.storage.currentInning + 1);
        } else {
            return self.getInningPlayers(defence, type, self.storage.currentInning + 1);
        }
    };


    self.register = function (controller) {
        controller.players = self;
    };
}