function ScoreEditorController() {
    'use strict';

    var self = this;
    var $editor = $('.js-scoreeditor');
    var $addContainer = $('.js-scoreeditor-tree-add');
    var $tree = $('.js-scoreeditor-tree div');
    var scrollBar = $('.js-scoreeditor-tree').jScrollPane().data('jsp');
    var selectedNode = null;
    var currentPlayerId = 0;
    var scoreTypes = {
        none: {},
        batters: {
            AB: 'AB - At Bat',
            R: 'R - Run',
            H: 'H - Hit',
            v2B: '2B - Double',
            v3B: '3B - Triple',
            HR: 'HR - Home Run',
            RBI: 'RBI - Run Batted In',
            BB: 'BB - Walk',
            HBP: 'HBP - Hit By Pitch',
            SO: 'SO - Strike Out',
            SF: 'SF - Sacrifice Fly',
            SH: 'SH - Sacrifice Bunt',
            SB: 'SB - Stolen Base',
            CS: 'CS - Caught Stealing',
            DP: 'DP - Double Play',
            E: 'E - Error',
            PA: 'PA - Plate Appearance'
        },
        pitchers: {
            PC: 'PC - Pitches Count',
            IP: '1/3 IP - Inning Pitch',
            H: 'H - Hit',
            R: 'R - Run',
            ER: 'ER - Earned Run',
            BB: 'BB - Walk',
            SO: 'SO - Strike Out',
            SV: 'SV - Save',
            v2B: '2B - Double',
            v3B: '3B - Triple',
            HR: 'HR - Home Run',
            BF: 'BF - Batter Faced',
            IBB: 'IBB - Intentional Walk',
            SHO: 'SHO - Shutout',
            WP: 'WP - Wild Pitch',
            HB: 'HB - Hit Batter',
            AO: 'F - Fly Out',
            LO: 'LO - Line Out',
            PO: 'PO - Pop Out',
            FBO: 'FBO - Foul Ball Out',
            IFR: 'IFR - Infield Fly Rule',
            GO: 'GO - Ground Out',
            FPS: 'FPS - First Pitch Strike',
            PCS: 'PCS - Pitcher\'s Called Strike',
            PSS: 'PSS - Pitcher\'s Swinging Strike',
            PFB: 'PFB - Pitcher\'s Foul Ball',
            PIP: 'PIP - Pitcher\'s In Play Pitch'
        },

        fielders: {
            INN: 'INN - Inning Played',
            PO: 'PO - Putout',
            A: 'A - Assist',
            E: 'E - Error',
            C_WP: 'WP - Wild Pitch'
        }
    };


    var filterString = '';

    function getScoreName(score) {
        var dict = {
            v2B: '2B',
            v3B: '3B',
            IP: '1/3 IP',
            C_WP: 'WP',
            AO: 'F'
        };
        return dict[score] || score;
    }

    var Node = (function () {
        _node.lastId = 0;
        _node.all = [];
        _node.removeAll = function () {
            $tree.children().remove();
            Node.all = [];
            Node.lastId = 0;
        };

        _node.collapseAll = function () {
            for (var i in _node.all) {
                _node.all[i].collapse();
            }
        };

        _node.expandAll = function () {
            for (var i in _node.all) {
                _node.all[i].expand();
            }
        };

        function _node(titleConf) {
            var node = {};
            var $children = null;
            var $title = null;
            var $expandIndicator = null;
            var expanded = false;

            titleConf = titleConf || {
                type1: 'home',
                text1: '',
                type2: 'visitor',
                text2: ''
            };

            function createDom() {
                $title = $('<div class="node-title js-scoreeditor-node">');
                $expandIndicator = $('<span>');
                $title.append($expandIndicator);

                if (typeof titleConf === 'object') {
                    $title
                        .append($('<span>').attr('type', titleConf.type1).text(titleConf.text1))
                        .append($('<span>').attr('type', titleConf.type2).text(titleConf.text2).css('margin-left', '5px'));
                } else {
                    $title.append($('<span>').text(titleConf));
                }

                $children = $('<div class="node-children">');
                var $main = $('<div class="node">')
                    .append($title)
                    .append($children);

                $children.hide();

                $title.click(function () {
                    node.onClick();
                });

                return $main;
            }

            function creation() {
                node.parent = null;
                node.children = [];
                node.$ = createDom();
                node.id = ++Node.lastId;
                Node.all.push(node);
            }

            node.onClick = function () {
                var isExpanded = expanded;
                $('.js-scoreeditor-node').removeAttr('selected');
                selectedNode = null;
                Node.collapseAll();

                if (isExpanded) {
                    if (node.parent !== null) {
                        node.parent.expand();
                    }
                    node.collapse();
                } else {
                    node.expand();
                }

                scrollBar.reinitialise();

                if (node.getType() == 'state' || node.getType() == 'score') {
                    $title.attr('selected', 1);
                    selectedNode = node;
                }
            };

            node.add = function (child) {
                node.children.push(child);
                $children.append(child.$);
                child.parent = node;
            };

            node.expand = function () {
                expanded = true;
                $children.show();

                $expandIndicator.text(node.children.length > 0 ? '-' : '');

                if (node.parent !== null) {
                    node.parent.expand();
                }
            };

            node.collapse = function () {
                expanded = false;
                $children.hide();
                $expandIndicator.text(node.children.length > 0 ? '+' : '');
            };

            node.setAttributes = function (obj) {
                for (var k in obj) {
                    node.$.attr(k, obj[k]);
                }
            };

            node.getAttributes = function (attrs) {
                var obj = {};
                for (var k in attrs) {
                    obj[attrs[k]] = node.$.attr(attrs[k]);
                }

                return obj;
            };

            node.getType = function () {
                return node.$.attr('type');
            };

            node.remove = function () {
                node.parent.children = node.parent.children.filter(function (n) {
                    return n.id !== node.id;
                });
                Node.all = Node.all.filter(function (n) {
                    return n.id !== node.id;
                });
                node.parent.expand();
                node.$.remove();
            };

            creation();
            return node;
        }

        return _node;
    })();

    function updateTree() {
        Node.removeAll();

        var filterRegexp = new RegExp('.*' + filterString + '.*');
        var lastRootNode = null;

        self.storage.forEach(['inning', 'state'], {
            inning: function (o) {
                lastRootNode = new Node('Inning ' + (o.i + 1));
                $tree.append(lastRootNode.$);
            },

            state: function (o) {

                var currentBatters = self.players.getInningPlayers(o.state.offence, 'batters', o.i + 1);
                var currentPitcher = self.players.getPlayer(o.state.pitcher, 'pitchers');

                var stateNode = new Node({
                    type1: o.state.offence,
                    type2: o.state.defence,
                    text1: o.state.offence.toUpperCase()[0] + 'Batter: ' + currentBatters[o.state.batter - 1].name,
                    text2: o.state.defence.toUpperCase()[0] + 'Pitcher: ' + currentPitcher.name
                });

                stateNode.setAttributes({
                    type: 'state',
                    inning: o.i,
                    state: o.s
                });
                lastRootNode.add(stateNode);

                var playerScores = o.state.playerScores.filter(function (s) {
                    return s.id == currentPlayerId && !!(getScoreName(s.score) + s.type).match(filterRegexp);
                });
                for (var s in playerScores) {
                    var score = playerScores[s];
                    var scoreNode = new Node(getScoreName(score.score) + ' for ' + score.type);
                    scoreNode.setAttributes({
                        type: 'score',
                        hash: score.score + score.type + score.id + score.pitch
                    });
                    stateNode.add(scoreNode);
                }
            }
        });

        Node.collapseAll();

        scrollBar.reinitialise();
    }

    function applyFilter() {
        filterString = $('.js-scoreeditor-input-filter').val();
        updateTree();
        Node.expandAll();
        scrollBar.reinitialise();
    }


    $('.js-lineup-pitcher[player], .js-lineup-batter[player]').click(function () {
        var $this = $(this);
        var player = self.players.getPlayer(+$this.attr('player'), 'batters') ||
            self.players.getPlayer(+$this.attr('player'), 'pitchers') ||
            self.players.getPlayer(+$this.attr('player'), 'fielders');

        var title = 'Score editor: ' + player.number + ' ' + player.name + ' ' + player.position;
        $editor.find('h1').text(title);

        $addContainer.hide();
        $('.js-scoreeditor-select-type').change();

        currentPlayerId = player.id;

        updateTree();
        selectedNode = null;
        $editor.show();
    });

    $('.js-scoreeditor-button-close').click(function () {
        $editor.hide();
        self.onEdited();
    });

    $('.js-scoreeditor-button-add').click(function () {
        $addContainer.show();
    });

    $('.js-scoreeditor-button-remove').click(function () {
        if (selectedNode === null) {
            window.warning('Score is not selected');
            return;
        }

        if (selectedNode.getType() != 'score') {
            window.warning('Only scores can be removed');
            return;
        }

        var stateAttrs = selectedNode.parent.getAttributes(['inning', 'state']);
        var attrs = selectedNode.getAttributes(['hash']);
        var isFound = 0;
        self.storage.removePlayerScore(+stateAttrs.inning, +stateAttrs.state, function (score) {
            return score.score + score.type + score.id + score.pitch == attrs.hash && isFound++ === 0;
        });

        selectedNode.remove();
        selectedNode = null;
        scrollBar.reinitialise();
    });

    $('.js-scoreeditor-button-save').click(function () {
        var type = $('.js-scoreeditor-select-type').val();
        var score = $('.js-scoreeditor-select-score').val();

        if (type === 'none') {
            window.warning('Score Category is not selected');
            return;
        }

        if (score === '') {
            window.warning('Score Type is not selected');
            return;
        }

        if (selectedNode === null) {
            window.warning('State is not selected');
            return;
        }

        var stateNode = selectedNode.getType() == 'score' ? selectedNode.parent : null;
        stateNode = stateNode === null && selectedNode.getType() == 'state' ? selectedNode : stateNode;

        if (stateNode === null) {
            window.warning('Invalid selection');
            return;
        }

        var stateAttrs = stateNode.getAttributes(['inning', 'state']);

        self.storage.addPlayerScore(currentPlayerId, type, score, +stateAttrs.inning, +stateAttrs.state);

        var scoreNode = new Node(getScoreName(score) + ' for ' + type);
        scoreNode.setAttributes({
            type: 'score',
            hash: score + type + currentPlayerId + 0
        });
        stateNode.add(scoreNode);
        stateNode.expand();

        scrollBar.reinitialise();
        $addContainer.hide();
    });

    $('.js-scoreeditor-button-cancel').click(function () {
        $addContainer.hide();
    });

    $('.js-scoreeditor-select-type').change(function () {
        var value = $(this).val();
        var $scoreSelect = $('.js-scoreeditor-select-score');
        $scoreSelect.children('option[score]').remove();
        $scoreSelect.change();
        for (var k in scoreTypes[value]) {
            $scoreSelect.append($('<option score="true">').val(k).text(scoreTypes[value][k]));
        }
    });

    $('.js-scoreeditor-input-filter')
        .change(function () {
            applyFilter();
        })
        .keydown(function (e) {
            if (e.which === 13) {
                applyFilter();
            }
        });

    /** @type PlayerController */
    self.players = null;
    /** @type StorageController */
    self.storage = null;

    self.onEdited = function () {
    };

}