function PitcherLimitController() {
    'use strict';

    var self = this;

    var inningCounter,
        totalCounter,
        innPCLimit,
        totalPCLimit;

    resetGlobals();
    function resetGlobals() {
        inningCounter = {};
        totalCounter = {};
        innPCLimit = window.G.pitcherPCLimits.innPCLimit;
        totalPCLimit = window.G.pitcherPCLimits.totalPCLimit;
    }

    function addPitcher(pitcherId) {
        if (!inningCounter[pitcherId]) {
            inningCounter[pitcherId] = {
                isSent: false,
                count: 0
            };
        }

        if (!totalCounter[pitcherId]) {
            totalCounter[pitcherId] = {
                isSent: false,
                count: 0
            };
        }
    }

    function trySend(pitcherId) {
        function send(counter, limit, type) {
            if (counter[pitcherId].count >= limit && !counter[pitcherId].isSent) {
                self.request.setPitcherPCLimit(pitcherId, type);
                counter[pitcherId].isSent = true;
            }
        }

        send(inningCounter, innPCLimit, 'innPCLimit');
        send(totalCounter, totalPCLimit, 'totalPCLimit');
    }

    function restore() {
        self.storage.forEach(['inning', 'state'], {
            inning: function () {
                inningCounter = {};
            },
            state: function (o) {
                for (var sc in o.state.playerScores) {
                    var score = o.state.playerScores[sc];
                    if (score.type == 'pitchers' && score.score == 'PC') {
                        addPitcher(score.id);

                        inningCounter[score.id].count++;
                        totalCounter[score.id].count++;
                    }
                }
            }
        });
    }

    /** @type RequestController */
    self.request = null;
    /** @type StorageController */
    self.storage = null;
    /** @type PlayerController */
    self.players = null;

    self.onPitch = function () {
        var pitcher = self.players.getCurrentFielder(1);

        addPitcher(pitcher.id);

        inningCounter[pitcher.id].count++;
        totalCounter[pitcher.id].count++;

        trySend(pitcher.id);
    };

    self.resetInning = function () {
        inningCounter = {};
    };

    self.restore = function () {
        resetGlobals();
        restore();
    };
}