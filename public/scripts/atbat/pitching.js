function PitchingController() {
    'use strict';

    var self = this;

    var pitchColors = {
        strike: 'blue',
        ball: 'green',
        out: 'red',
        hit: 'yellow',
        misc: 'yellow',
        foul: 'gray'
    };

    var pitchingState,
        isPitchingEvent,
        pitchingType,
        strikeType,
        pitchCounter,
        isEnabled,
        $lastPoint,
        isTrackerEnabled,
        isTrackerDisabled,
        createNewPitch;

    resetGlobals();
    function resetGlobals() {
        pitchingState = 'none'; // none, pitch, menu
        isPitchingEvent = false;
        pitchingType = null;
        strikeType = null;
        pitchCounter = {
            total: 0,
            strike: 0,
            ball: 0,
            foul: 0,
            out: 0
        }; // state
        isEnabled = false;
        $lastPoint = null;
        isTrackerEnabled = window.G.isPitchTrackingEnabled;
        isTrackerDisabled = !window.G.isPitchTrackingEnabled;

        createNewPitch = false;

        $('.js-pitch-region').children('.ui-circle').remove();
    }

    function updateEvents() {
        if (pitchCounter.strike == 3) {
            self.enable(false);
            self.on3Strikes(strikeType);
        }
        if (pitchCounter.ball == 4) {
            self.enable(false);
            self.on4Balls();
        }
    }

    function updatePitchUi() {
        var pitchScores = $('.js-pitch-type');

        function update($el, i, type) {
            if (i < pitchCounter[type]) {
                $el.attr('color', pitchColors[type]);
            } else {
                $el.removeAttr('color');
            }
        }

        pitchScores.eq(0).children('.ui-circle').each(function (i) {
            update($(this), i, 'ball');
        });
        pitchScores.eq(1).children('.ui-circle').each(function (i) {
            update($(this), i, 'strike');
        });
        pitchScores.eq(2).children('.ui-circle').each(function (i) {
            update($(this), i, 'out');
        });
    }

    function addPitch(type, action) {

        if (type == 'foul' && pitchCounter.strike < 2) {
            pitchCounter.strike++;
        } else {
            pitchCounter[type]++;
        }

        pitchCounter.total++;

        if (pitchCounter.strike < 3) {
            self.onAddLog(type, action);
        }

        self.storage.updatePitch({
            type: type,
            type2: action,
            counter: pitchCounter
        });

        updatePitchUi();
        updateEvents();
    }

    function setPitch(type, val) {
        pitchCounter.total -= pitchCounter[type];
        pitchCounter[type] = val;
        pitchCounter.total += pitchCounter[type];

        updatePitchUi();
    }

    function addHitPitch(type) {
        self.onAddLog('hit', type);

        self.storage.updatePitch({
            type: 'hit',
            type2: type
        });

        if ($lastPoint) {
            $lastPoint.attr('color', pitchColors.hit);
        }

        self.onPitch(type);

        isPitchingEvent = false;
        pitchingType = null;
        $lastPoint = null;
    }

    function addFoulPitch(type) {
        self.onAddLog('misc', type);

        pitchCounter.total++;

        self.storage.updatePitch({
            type: 'misc',
            type2: type,
            counter: pitchCounter
        });

        if ($lastPoint) {
            $lastPoint.attr('color', pitchColors.misc);
        }

        self.onPitch(type);

        createNewPitch = true;
    }

    function addOutPitch(type) {
        self.onAddLog('out', type);

        if ($lastPoint) { //if strikeout $lastPoint == null
            $lastPoint.attr('color', pitchColors.out);
        }

        if (type != "KS" && type != "KC") {
            self.onPitch(type);
        }

        isPitchingEvent = false;
        pitchingType = null;
        $lastPoint = null;
    }

    function restoreCounter() {
        var state = self.storage.getState();
        var pitch = self.storage.getPitch();
        if (pitch) {
            $.extend(pitchCounter, pitch.counter);
        }
        pitchCounter.out = state.outs || 0;
        updatePitchUi();
    }

    function restorePitches() {
        var $region = $('.js-pitch-region');
        var regionOffset = $region.offset();

        self.storage.forEach(['pitch'], {
            pitch: function (o) {
                var pitch = o.pitch;

                // If number undefined - pitch never exists
                if (pitch.number === undefined) {
                    return;
                }

                if (pitch.coordinatesCatch && isTrackerEnabled) {
                    $lastPoint = $('<div class="ui-circle" hastext="1">')
                        .text(pitch.number)
                        .appendTo($region)
                        .offset({
                            top: pitch.coordinatesCatch.y + regionOffset.top,
                            left: pitch.coordinatesCatch.x + regionOffset.left
                        });
                } else {
                    $lastPoint = $('<div>');
                }

                if (pitch.type) {
                    $lastPoint.attr('color', pitchColors[pitch.type]);
                    $lastPoint = null;
                } else {
                    // TODO: Logic is not correct (empty o.pitch at new state)
                    isPitchingEvent = true;
                    createNewPitch = false;

                    if (isTrackerEnabled) {
                        self.onEnable(true);
                    } else {
                        self.onBeforePitch();
                    }
                }
            }
        });

        // TODO: Logic is not correct
        if (isTrackerDisabled) {
            $lastPoint = $('<div>');
        }
    }

    function tryAddPitchUi(type, action) {
        if (!isEnabled) {
            window.warning('This option is disabled');
            return false;
        }

        if (pitchCounter.strike >= 3 || pitchCounter.ball >= 4) {
            return false;
        }

        if (!isPitchingEvent || !$lastPoint) {
            return false;
        }


        if (isTrackerDisabled) {
            //self.storage.newPitch();

            addPitch(type, action);

            self.storage.updatePitch({
                number: pitchCounter.total
            });
            self.onPitch(type == 'strike' ? strikeType : type);

            // Used in reset but also for each pitch
            // if 3 strikes then createNewPitch will be true on reset
            if (pitchCounter.strike < 3) {
                createNewPitch = true;
            }
            return true;
        }

        pitchingType = type;
        $lastPoint.attr('color', pitchColors[pitchingType]);

        addPitch(type, action);

        self.storage.updatePitch({
            number: pitchCounter.total
        });

        self.onPitch(type == 'strike' ? strikeType : type);

        isPitchingEvent = false;
        pitchingType = null;
        $lastPoint = null;
        pitchingState = 'none';
        self.onEnable(false);

        // Used in reset but also for each pitch
        // if 3 strikes then createNewPitch will be true on reset
        if (pitchCounter.strike < 3) {
            createNewPitch = true;
        }

        //if (isTrackerDisabled){
        //    isPitchingEvent = true;
        //    pitchingState = 'pitch';
        //    self.onBeforePitch();
        //    $lastPoint = $('<div>');
        //    self.onEnable(true);
        //}
        return true;
    }

    function initMenu() {
        function disabled(){
            return !isEnabled || pitchCounter.strike >= 3 || pitchCounter.ball >= 4 || !isPitchingEvent || !$lastPoint;
        }

        function handler(action, type) {
            // TODO: Should be rewriten
            if (!isEnabled) {
                window.warning('This option is disabled');
                return false;
            }

            if (pitchCounter.strike >= 3 || pitchCounter.ball >= 4) {
                return false;
            }

            if (!isPitchingEvent || !$lastPoint) {
                return false;
            }

            if (type == 'strike') {
                strikeType = 'KS';
            }

            if (action == 'HBP' && (type == 'strike' && pitchCounter.strike < 2 || type == 'ball' && pitchCounter.ball < 3)) {
                self.onBeforeHitByPitch();
            }

            if (action == 'HBP' && type == 'ball' && pitchCounter.ball == 3){
                action = null;
            }

            if (tryAddPitchUi(type, action)) {
                if (type == 'strike' && pitchCounter.strike < 3) {
                    if (action == 'WP') {
                        self.onWildPitch();
                    }
                    if (action == 'HBP') {
                        self.onHitByPitch();
                    }
                }

                if (type == 'ball' && pitchCounter.ball < 4) {
                    if (action == 'WP') {
                        self.onWildPitch();
                    }
                    if (action == 'HBP') {
                        self.onHitByPitch();
                    }
                }
            }
            return true;
        }

        $.contextMenu({
            selector: '.js-button-field-hitbypitch',
            trigger: 'left',
            items: {
                'ball': {
                    name: 'Hit by Pitch - Ball',
                    callback: handler.bind(null, 'HBP'),
                    disabled: disabled
                },
                'strike': {
                    name: 'Hit by Pitch - Strike',
                    callback: handler.bind(null, 'HBP'),
                    disabled: disabled
                }
            }
        });

        $.contextMenu({
            selector: '.js-button-field-out-wildpitch',
            trigger: 'left',
            items: {
                'ball': {
                    name: 'Wild Pitch - Ball',
                    callback: handler.bind(null, 'WP'),
                    disabled: disabled
                },
                'strike': {
                    name: 'Wild Pitch - Strike',
                    callback: handler.bind(null, 'WP'),
                    disabled: disabled
                }
            }
        });
    }

    $('.js-button-pitch').click(function () {
        if (isEnabled && pitchingState == 'none') {
            isPitchingEvent = true;
            pitchingState = 'pitch';
            self.onBeforePitch();
        }
    });

    //$('.js-pitch-type').click(function(){
    //    if (!isPitchingEvent || $(this).attr('type') == 'out' ||
    //        isTrackerDisabled || !$lastPoint){
    //        return;
    //    }
    //
    //    pitchingType = $(this).attr('type');
    //    $lastPoint.attr('color',pitchColors[pitchingType]);
    //
    //    addPitch(pitchingType);
    //
    //    //self.onPitch(pitchingType); //can add
    //
    //    isPitchingEvent = false;
    //    pitchingType = null;
    //    $lastPoint = null;
    //    pitchingState = pitchCounter.strike == 3? 'menu': 'none';
    //    self.onEnable(false);
    //});

    //$('.js-pitch-type .ui-circle[value]').click(function(){
    //    if (isTrackerEnabled || !isPitchingEvent)
    //        return;
    //
    //    var $current = $(this);
    //    var $parent = $current.parent();
    //
    //    setPitch($parent.attr('type'), +$current.attr('value'));
    //});

    $('.js-pitch-region').mousedown(function (e) {
        if (isEnabled && isPitchingEvent && !$lastPoint && isTrackerEnabled) {
            var regionOffset = $(this).offset();
            var x = e.pageX - regionOffset.left - 10;
            var y = e.pageY - regionOffset.top - 10;

            $lastPoint = $('<div class="ui-circle" hastext="1">')
                .text(pitchCounter.total + 1)
                .appendTo(this)
                .offset({
                    top: y + regionOffset.top,
                    left: x + regionOffset.left
                });

            self.storage.updatePitch({

                coordinatesCatch: {
                    x: x,
                    y: y
                },
                number: pitchCounter.total + 1,
                counter: pitchCounter
            });

            //self.onPitch();

            pitchingState = 'menu';
            self.onEnable(true);
        }
    });

    $('.js-button-field-ball').click(function () {
        tryAddPitchUi('ball');
    });

    $('.js-button-field-strike-looking').click(function () {
        strikeType = 'KC';
        tryAddPitchUi('strike');
    });

    $('.js-button-field-strike-swinging').click(function () {
        strikeType = 'KS';
        tryAddPitchUi('strike');
    });

    $('.js-button-field-ball-foul').click(function () {
        tryAddPitchUi('foul');
    });

    self.on4Balls = function () {
    };
    self.on3Strikes = function () {
    };
    self.on3Outs = function () {
    };
    self.onBeforePitch = function () {
    };
    self.onPitch = function () {
    };
    self.onEnable = function (isEnabled) {
    };
    self.onAddLog = function (type, text) {
    };
    self.onWildPitch = function () {
    };
    self.onBeforeHitByPitch = function () {
    };

    self.onHitByPitch = function () {
    };


    self.reset = function () {
        pitchCounter.out = 0;
        self.resetPitching();
    };

    self.resetPitching = function () {
        pitchCounter.strike = 0;
        pitchCounter.ball = 0;
        pitchCounter.foul = 0;
        pitchCounter.total = 0;
        updatePitchUi();

        $('.js-pitch-region').children('.ui-circle').remove();

        isPitchingEvent = false;
        pitchingType = null;
        $lastPoint = null;
        pitchingState = 'none';

        self.enable(true);
        self.onEnable(false);

        createNewPitch = true;

        // TODO: Duplicated code line 212
        if (isTrackerDisabled) {
            isPitchingEvent = true;
            pitchingState = 'pitch';
            self.onBeforePitch();
            $lastPoint = $('<div>');
            self.onEnable(true);
        }
    };

    self.addOut = function () {

        // Current state can not have 3 outs (Undo problem)
        // storage monitor outs by pitch counter
        //self.storage.updateState({ outs: pitchCounter.out });

        pitchCounter.out++;

        if (pitchCounter.out == 3) {
            self.enable(false);
            self.on3Outs();
        }

        self.storage.updatePitch({counter: pitchCounter});

        updatePitchUi();
    };

    self.addHitPitch = function (type) {
        addHitPitch(type);
    };

    self.addFoulPitch = function (type) {
        addFoulPitch(type);
    };

    self.addOutPitch = function (type) {
        addOutPitch(type);
    };

    self.checkPitchingEvent = function () {
        return isPitchingEvent;
    };

    self.enable = function (isEnable) {
        var button = $('.js-button-pitch');
        if (isEnable) {
            button.removeAttr('disabled');
        } else {
            button.attr('disabled', 1);
            button.contextMenu(false);
        }


        isEnabled = isEnable;
    };

    self.restore = function () {
        resetGlobals();
        self.reset();
        restoreCounter();
        restorePitches();
    };

    self.tryCreatePitch = function () {
        if (createNewPitch) {
            createNewPitch = false;
            self.storage.newPitch();
        }
    };

    /**
     * Activate new pitch creation on request
     */
    self.passCurrentPitch = function () {
        createNewPitch = true;
    };

    self.is3Outs = function () {
        return pitchCounter.out == 3;
    };

    initMenu();

    return self;
}