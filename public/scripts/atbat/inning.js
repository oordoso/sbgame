function InningController() {
    'use strict';

    var self = this;

    var EXTRA_GAME_INNING = window.G.extraGameInning,
        REGULAR_GAME_INNING = window.G.regularGameInning;

    var $lineups,
        defenceLineup,
        offenceLineup,
        $defenceLineup,
        $offenceLineup,

        gameStarted,

        currentPitcher,
        currentBatter,
        nextBatter,
        currentInning,
        winner,
        outs3,
        lastBatterNumber,

        inningCounter,

        inningTotal,

        scoring,
        $scoring,

        createNextState;

    resetGlobals();

    function resetGlobals() {
        gameStarted = false;

        currentPitcher = null; // state
        currentBatter = null; // state
        nextBatter = null; // state
        currentInning = 0; // 1-9 // state
        winner = null; // state
        outs3 = false;
        lastBatterNumber = 1;

        inningCounter = {
            visitor: 0,
            home: 0
        }; // state

        inningTotal = {
            visitor: {R: 0, H: 0, E: 0},
            home: {R: 0, H: 0, E: 0}
        }; // state

        scoring = null; // state
        $scoring = null;

        createNextState = false;
    }

    function initScoring() {
        scoring = {
            batters: {},
            pitchers: {},
            fielders: {}
        };

        $scoring = {
            batters: {},
            pitchers: {},
            fielders: {}
        };


        function addBatters(team) {
            var batters = self.players.getPlayers(team, 'batters');
            for (var pi in batters) {
                var player = batters[pi];
                $scoring.batters[player.id] = $lineups[team].find('.js-lineup-batter[player=' + player.id + ']');
                $scoring.batters[player.id].children('td[type]').text(0);
                scoring.batters[player.id] = {
                    player: player,
                    AB: 0,
                    R: 0,
                    H: 0,
                    RBI: 0,
                    BB: 0,
                    SO: 0,
                    HR: 0,
                    HBP: 0,
                    v2B: 0,
                    v3B: 0,
                    SB: 0,
                    CS: 0,
                    SAC: 0,
                    SH: 0,
                    SF: 0,
                    PA: 0
                };
            }
        }

        function addPitchers(team) {
            var pitchers = self.players.getPlayers(team, 'pitchers');
            for (var pi in pitchers) {
                var player = pitchers[pi];
                $scoring.pitchers[player.id] = $lineups[team].find('.js-lineup-pitcher[player=' + player.id + ']');
                $scoring.pitchers[player.id].children('td[type]').text(0);
                scoring.pitchers[player.id] = {
                    player: player,
                    IP: 0,
                    PC: 0,
                    H: 0,
                    R: 0,
                    ER: 0,
                    BB: 0,
                    SO: 0,
                    HR: 0,
                    HB: 0,
                    CS: 0,
                    SB: 0,
                    SF: 0,
                    SH: 0,
                    AO: 0,
                    LO: 0,
                    PO: 0,
                    FBO: 0,
                    IFR: 0,
                    GO: 0,
                    FPS: 0,
                    PCS: 0,
                    PSS: 0,
                    PFB: 0,
                    PIP: 0
                };
            }
        }

        function addFielders(team) {
            var fielders = self.players.getPlayers(team, 'fielders');
            for (var pi in fielders) {
                var player = fielders[pi];
                // TODO: Fix this mock
                $scoring.fielders[player.id] = $();
                scoring.fielders[player.id] = {
                    player: player,
                    GS: 0,
                    INN: 0,
                    PO: 0,
                    A: 0,
                    DP: 0,
                    TP: 0,
                    PB: 0,
                    C_WP: 0,
                    E: 0
                };
            }
        }

        addBatters('home');
        addPitchers('home');
        addFielders('home');
        addBatters('visitor');
        addPitchers('visitor');
        addFielders('visitor');
    }

    function restoreScoring() {
        self.storage.forEach(['inning', 'state'], {
            state: function (o) {
                for (var sc in o.state.playerScores) {
                    var score = o.state.playerScores[sc];
                    scoring[score.type][score.id] = scoring[score.type][score.id] || {};
                    scoring[score.type][score.id][score.score] = scoring[score.type][score.id][score.score] || 0;
                    scoring[score.type][score.id][score.score]++;
                }
            }
        });
    }


    function addScore(player, playertype, scoretype) {
        var val = ++scoring[playertype][player.id][scoretype];

        self.storage.addPlayerScore(player.id, playertype, scoretype);

        //// TODO: Replace UI update
        //$scoring[playertype][player.id]
        //    .children('td[type='+scoretype+']')
        //    .text(val);
    }


    function initLineups() {
        $lineups = $('.js-lineup');
        $lineups.visitor = $lineups.eq(0).add($lineups.eq(2));
        $lineups.home = $lineups.eq(1).add($lineups.eq(3));

        var lineups = {
            home: self.players.getInningLineup('home', currentInning),
            visitor: self.players.getInningLineup('visitor', currentInning)
        };

        var state = self.storage.getState();
        if (state.offence && state.defence) {

            defenceLineup = lineups[state.defence];
            offenceLineup = lineups[state.offence];
            $defenceLineup = $lineups[state.defence];
            $offenceLineup = $lineups[state.offence];

            currentPitcher = defenceLineup.pitchers[0];

        } else {

            defenceLineup = lineups.home;
            offenceLineup = lineups.visitor;
            $defenceLineup = $lineups.home;
            $offenceLineup = $lineups.visitor;

            currentPitcher = defenceLineup.pitchers[0];

            self.storage.updateState({
                offence: offenceLineup.type,
                defence: defenceLineup.type,
                pitcher: currentPitcher.id,
                outs: 0
            });
        }
    }

    function resetInningCounter() {
        inningCounter.visitor = 0;
        inningCounter.home = 0;
    }

    function restoreInningCounters() {
        self.storage.forEach(['inning', 'state'], {
            inning: function (o) {
                inningCounter.visitor = 0;
                inningCounter.home = 0;
            },

            state: function (o) {
                for (var sc in o.state.inningScores) {
                    var score = o.state.inningScores[sc];

                    if (score.score == 'R') {
                        inningCounter[score.type]++;
                    }
                    inningTotal[score.type][score.score]++;
                }
            },

            inningAfter: function (o) {
                updateTeamScoreUi(o.i + 1);
            }
        });
    }

    function setCurrentBatter(number) {
        currentBatter = offenceLineup.batters[number - 1];
        if (number < 9) {
            nextBatter = offenceLineup.batters[number];
        } else {
            nextBatter = offenceLineup.batters[0];
        }

        self.storage.updateState({batter: number});

        updateInningBatterUi();
    }

    function restoreCurrentBatter() {
        var state = self.storage.getState();
        setCurrentBatter(state.batter);
    }

    function restoreLastBatter() {
        lastBatterNumber = 1;

        self.storage.forEach(['state'], {
            state: function (o) {
                if (o.state.lastBatterNumber) {
                    lastBatterNumber = o.state.lastBatterNumber;
                }
            }
        });
    }

    function switchLineups() {

        self.storage.updateState({
            offence: defenceLineup.type,
            defence: offenceLineup.type
        });

        var buffer = self.players.getInningLineup(defenceLineup.type, currentInning);
        defenceLineup = self.players.getInningLineup(offenceLineup.type, currentInning);
        offenceLineup = buffer;
        buffer = $defenceLineup;
        $defenceLineup = $offenceLineup;
        $offenceLineup = buffer;

        currentPitcher = defenceLineup.pitchers[0];

        self.storage.updateState({
            pitcher: currentPitcher.id
        });
    }

    function nextInning() {
        resetInningCounter();
        currentInning++;
        updateTeamScoreUi(currentInning);
    }

    function updateHeader() {
        var side = '';
        if (offenceLineup.type == 'visitor') {
            side = 'TOP';
        } else {
            side = 'BOTTOM';
        }

        var ending = [0, 'ST', 'ND', 'RD'];
        if (ending[currentInning]) {
            ending = ending[currentInning];
        } else {
            ending = 'TH';
        }


        $('.js-header-second').text(side + ' OF THE ' + currentInning + ending + ' / ' + offenceLineup.name);
    }

    function updateInningTableHead() {
        var $heads = $('.ui-table-statistic-head-color');
        $heads.removeAttr('selected');
        if (offenceLineup.type == 'visitor') {
            $heads.eq(0).attr('selected', 1);
        } else {
            $heads.eq(1).attr('selected', 1);
        }
    }

    function updateInningBatterUi() {
        $('.js-lineup-batter').removeAttr('selected');
        $('.js-lineup-pitcher').removeAttr('selected');
        $('.js-batting-batside').html();

        if (currentBatter) {
            $('.js-lineup-batter[player=' + currentBatter.id + ']', $offenceLineup).attr('selected', 1);
            $('.js-lineup-pitcher[player=' + currentPitcher.id + ']', $defenceLineup).attr('selected', 1);

            $('.js-label-atbat').text(currentBatter.name);
            $('.js-label-ondeck').text(nextBatter === null ? '' : nextBatter.name);

            if (currentBatter.bats == 'R') {
                $('.js-batting-batside').html('<span class="ui-triangle" type="left"></span>BATTING RIGHT');
            }
            if (currentBatter.bats == 'L') {
                $('.js-batting-batside').html('BATTING LEFT<span class="ui-triangle" type="right"></span>');
            }
        }
    }

    function updateTeamScoreUi(inning) {
        function getScoreGatter(score, isDefenceHome) {
            return function (i) {
                var id = (inning - 1) % count;
                if (i > id || i == id && isDefenceHome) {
                    $(this).text('');
                    return;
                }

                if (i == id) {
                    $(this).text(score);
                }
            };
        }

        var count = $('.js-inning-visitor').length;
        var startInning = (Math.floor((inning - 1) / count) * count) + 1;

        $('.js-inning-visitor-head').each(function (i) {
            $(this).text(startInning + i);
        });

        $('.js-inning-visitor').each(getScoreGatter(inningCounter.visitor));
        $('.js-inning-home').each(getScoreGatter(inningCounter.home, defenceLineup.type == 'home'));
        $('.js-inning-visitor-r').text(inningTotal.visitor.R);
        $('.js-inning-visitor-h').text(inningTotal.visitor.H);
        $('.js-inning-visitor-e').text(inningTotal.visitor.E);
        $('.js-inning-home-r').text(inningTotal.home.R);
        $('.js-inning-home-h').text(inningTotal.home.H);
        $('.js-inning-home-e').text(inningTotal.home.E);
    }

    function updateLineupScoreUi() {
        function convert(type, value, playerScores) {
            if (type == 'IP') {
                return Math.floor(value / 3) + '.' + (value % 3);
            }

            if (type == 'PC') {
                return value + ' (' + (playerScores.PFB) + '/' + (playerScores.PCS + playerScores.PSS) + ')';
            }

            return value;
        }

        function _byPlayersType(players, type) {
            for (var p in players) {
                var player = players[p];
                var $tds = $scoring[type][player.id].children('td');
                var counter = 2;
                for (var s in scoring[type][player.id]) {
                    if (s != 'player') {
                        $tds.eq(counter).text(convert(s, scoring[type][player.id][s], scoring[type][player.id]));
                        counter++;
                    }
                }
            }
        }

        _byPlayersType(self.players.getPlayers('visitor', 'batters'), 'batters');
        _byPlayersType(self.players.getPlayers('home', 'batters'), 'batters');
        _byPlayersType(self.players.getPlayers('visitor', 'pitchers'), 'pitchers');
        _byPlayersType(self.players.getPlayers('home', 'pitchers'), 'pitchers');
    }

    function getWinner() {
        if (offenceLineup.type == 'visitor' && inningTotal.visitor.R < inningTotal.home.R) {
            return 'home';
        }
        if (offenceLineup.type == 'home') {
            if (inningTotal.visitor.R < inningTotal.home.R) {
                return 'home';
            }
            if (inningTotal.visitor.R > inningTotal.home.R) {
                return 'visitor';
            }
            if (inningTotal.visitor.R == inningTotal.home.R) {
                return null;
            }
        }

        return null;
    }

    function checkGameEndCondition() {
        if (currentInning >= EXTRA_GAME_INNING && outs3) {
            winner = getWinner();
            return !!winner;
        }
        return false;
    }

    function checkRegularGameEndCondition() {
        if (currentInning >= REGULAR_GAME_INNING) {
            winner = getWinner();
            return !!winner;
        }
        return false;
    }

    function nextGameSet() {
        if (winner) {
            return;
        }

        self.onBeforeNextGameSet();

        // Next set should be created
        createNextState = true;

        if (checkGameEndCondition()) {
            self.endGame();
            return;
        }

        if (offenceLineup.type == 'home') {
            self.storage.nextInning();
            createNextState = false;

            nextInning();
        }
        switchLineups();
        self.onNextGameSet();

        var batterNumber = nextBatter.batter;
        setCurrentBatter(lastBatterNumber);
        lastBatterNumber = batterNumber;

        self.storage.updateState({lastBatterNumber: lastBatterNumber});

        self.onBatterReady(currentBatter);
        updateHeader();
        updateInningTableHead();
        updateTeamScoreUi(currentInning);
        updateLineupScoreUi();
    }

    /** @type PlayerController */
    self.players = null;
    /** @type StorageController */
    self.storage = null;

    self.onBatterReady = function () {
    };

    self.onBeforeNextGameSet = function () {
    };

    self.onNextGameSet = function () {
    };

    self.onGameEnd = function () {
    };

    self.onGameSuspend = function () {
    };

    self.register = function (controller) {
        controller.innings = self;
    };

    self.addInningRunScore = function () {
        var lineupType = offenceLineup.type;
        inningCounter[lineupType]++;
        inningTotal[lineupType].R++;

        self.storage.addInningScore(lineupType, 'R');

        updateTeamScoreUi(currentInning);
    };

    self.addInningHitScore = function () {
        var lineupType = offenceLineup.type;
        inningTotal[lineupType].H++;

        self.storage.addInningScore(lineupType, 'H');

        updateTeamScoreUi(currentInning);
    };

    self.addInningErrorScore = function () {
        var lineupType = defenceLineup.type;
        inningTotal[lineupType].E++;

        self.storage.addInningScore(lineupType, 'E');

        updateTeamScoreUi(currentInning);
    };

    self.addBothScore = function (scoretype) {
        self.addBatterScore(scoretype);
        self.addPitchScore(scoretype);
    };

    self.addBatterScore = function (batter, scoretype) {
        if (typeof batter == 'string') {
            scoretype = batter;
            batter = currentBatter;
        }
        addScore(batter, 'batters', scoretype);
    };

    self.addFielderScore = function (fielder, scoretype) {
        addScore(fielder, 'fielders', scoretype);
    };

    self.addPitchScore = function (scoretype) {
        addScore(currentPitcher, 'pitchers', scoretype);
    };

    self.tryDoNextGameSet = function () {
        if (outs3) {

            // Do not create next state before scores fulfillment
            createNextState = false;

            nextGameSet();
            outs3 = false;
            return true;
        }
        return false;
    };

    self.nextBatter = function () {
        if (winner) {
            return;
        }

        createNextState = true;
        if (self.tryDoNextGameSet()) {
            return;
        }

        setCurrentBatter(nextBatter.batter);
        self.onBatterReady(currentBatter);
    };

    self.getGameSet = function () {
        return {
            defenceLineup: defenceLineup,
            offenceLineup: offenceLineup
        };
    };

    self.getCurrentBatter = function () {
        return currentBatter;
    };

    self.set3Outs = function () {
        outs3 = true;
    };

    self.updateLineup = function () {
        defenceLineup = self.players.getInningLineup(defenceLineup.type, currentInning);
        offenceLineup = self.players.getInningLineup(offenceLineup.type, currentInning);
        currentPitcher = defenceLineup.pitchers[0];

        self.storage.updateState({
            pitcher: currentPitcher.id
        });

        updateInningBatterUi();
        updateLineupScoreUi();
    };

    self.updatePitchActors = function () {
        self.storage.updatePitch({
            pitcher: currentPitcher.id,
            batter: currentBatter.id
        });
    };

    self.updateScores = function () {
        if (!gameStarted) {
            return;
        }

        initScoring();
        restoreScoring();
        updateLineupScoreUi();
    };

    self.startGame = function () {
        if (gameStarted) {
            location.reload(); // TODO: Replace from here
        }

        currentInning = 1;

        initLineups();
        initScoring();
        updateHeader();
        updateInningTableHead();
        updateLineupScoreUi();
        self.onNextGameSet();

        resetInningCounter();
        updateTeamScoreUi(currentInning);

        setCurrentBatter(1);
        self.onBatterReady(currentBatter);

        gameStarted = true;
    };

    self.endGame = function (status) {
        status = status || (currentInning > EXTRA_GAME_INNING ? 3 : 2);
        switch (status) {
            case 2: //End - Regulation
                if (currentInning < EXTRA_GAME_INNING) {
                    window.warning('You can not do this action until full ' + EXTRA_GAME_INNING + ' innings completed');
                    return false; // Used to tell about error
                }

                if (currentInning > EXTRA_GAME_INNING) {
                    window.warning('You can not do this action when extra innings began');
                    return false;
                }

                if (!checkGameEndCondition()) {
                    window.warning('You can not do this action. The winner is undefined');
                    return false;
                }
                break;
            case 3: //End - ExtraInnings
                if (currentInning <= EXTRA_GAME_INNING) {
                    window.warning('You can not do this action until extra innings begin');
                    return false;
                }

                if (!checkGameEndCondition()) {
                    window.warning('You can not do this action. The winner is undefined');
                    return false;
                }
                break;
            case 4: //End - RainCounts
                if (currentInning < REGULAR_GAME_INNING) {
                    window.warning('You can not do this action until ' + REGULAR_GAME_INNING + 'th inning begin');
                    return false;
                }

                if (!checkRegularGameEndCondition()) {
                    window.warning('You can not do this action. The winner is undefined');
                    return false;
                }
                break;
        }

        if (status >= 5 && status <= 6) {
            self.onGameSuspend(status, 'suspended');
            return;
        }

        if (status >= 7 && status <= 9) {
            self.onGameSuspend(status, 'delayed');
            return;
        }

        //currentBatter = null;
        //updateInningBatterUi();

        self.onGameEnd(status, getWinner());
    };

    self.restore = function () {
        resetGlobals();

        currentInning = self.storage.currentInning + 1;

        initLineups();
        initScoring();
        restoreScoring();
        updateHeader();
        updateInningTableHead();
        updateLineupScoreUi();

        restoreInningCounters();
        restoreCurrentBatter();
        restoreLastBatter();

        gameStarted = true;
    };

    self.tryCreateState = function () {
        if (createNextState) {
            createNextState = false;
            self.storage.nextState();
        }
    };

    return self;
}