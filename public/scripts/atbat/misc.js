function MiscController() {
    'use strict';

    var self = this;

    var stealType;
    var isEnabled;

    function initMenu() {
        var items = {
            'SB': {
                name: 'Stolen Base',
                callback: menuHandle,
                disabled: disableHandle
            },
            'CS': {
                name: 'Caught Stealing',
                callback: menuHandle,
                disabled: disableHandle
            }
        };

        $.contextMenu({
            selector: '.js-button-field-stealing',
            trigger: 'left',
            items: items
        });

        self.enable(true);
    }

    function menuHandle(item) {
        if (!isStealAllowed()) {
            window.warning('You can not do this action. The action limited by number of saved bases.');
            return;
        }

        stealType = item;
        $('.js-field').attr('active', 1);
    }

    function isStealAllowed() {
        var baseState = self.storage.getBaseState();
        var filled = baseState.bases.filter(function (x) {
            return x;
        }).length;
        return filled > 0;
    }

    function disableHandle(item) {
        return !isEnabled;
    }


    /** @type StorageController */
    self.storage = null;

    self.onOut = function (batter) {
    };
    self.onBatterBase = function (batter) {
    };

    self.enable = function (isEnable) {
        if (isEnable === undefined) {
            return isEnabled;
        }

        isEnabled = isEnable;
        var button = $('.js-button-steal');
        button.contextMenu(isEnable);

        if (isEnable) {
            button.removeAttr('disabled');
        } else {
            button.attr('disabled', 1);
            stealType = null;
        }
    };

    self.menuHandle = function (type) {
        return isEnabled && (menuHandle(type) || true);
    };

    self.doClick = function (obj) {
        if (isEnabled && stealType) {
            if (obj.type == 'fielder') {
                window.warning('You should select batters');
                return;
            }

            if (obj.type == 'batter') {
                var baseState = self.storage.getBaseState();
                if (obj.position < 3 && !!baseState.bases[obj.position + 1]) {
                    window.warning('You can not do this action. The base is occupied.');
                    return;
                }

                if (obj.position === 0) {
                    window.warning('You can not do this action. The batter is at bat.');
                    return;
                }


                if (stealType == 'SB') {

                    self.storage.updatePitch({
                        type: 'misc',
                        type2: 'SB'
                    });

                    self.onBatterBase(obj);
                }

                if (stealType == 'CS') {

                    self.storage.updatePitch({
                        type: 'out',
                        type2: 'CS',
                        batters: [obj.position]
                    });

                    self.onOut(obj);
                }

                stealType = null;
                $('.js-field').removeAttr('active');
            }
        }
    };

    initMenu();
}