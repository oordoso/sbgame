function AdvanceController() {
    'use strict';

    var self = this;

    var advancedBy;
    var isEnabled;
    var $bases = $('.js-field-bases .base');
    var selectedRunner;
    var isActivated;

    resetGlobal();
    function resetGlobal() {
        selectedRunner = null;
        isActivated = false;

    }

    function menuHandle(item) {
        isActivated = true;
        $('.js-field').attr('active', 1);
    }


    $bases.click(function () {
        if (isEnabled && isActivated && selectedRunner !== null) {
            var type = +$(this).attr('base');

            self.onAdvancing(selectedRunner.position, type);

            selectedRunner = null;
            isActivated = false;
            $('.js-field').removeAttr('active');
        }
    });

    self.onAdvancing = function (from, to) {
    };

    self.enable = function (isEnable) {
        if (isEnable === undefined) {
            return isEnabled;
        }

        isEnabled = isEnable;

        if (!isEnable) {
            isActivated = false;
            selectedRunner = null;
        }
    };

    self.menuHandle = function (type) {
        return isEnabled && (menuHandle(type) || true);
    };

    self.doClick = function (obj) {
        if (isEnabled && isActivated) {
            if (obj.type == 'batter') {
                if (obj.position === 0) {
                    window.warning("Batter can not be advanced");
                    return;
                }
                selectedRunner = obj;
                return;
            }

            if (obj.type == 'fielder') {
                window.warning("Fielder can not be advanced");
            }
        }
    };

    self.enable(false);
}