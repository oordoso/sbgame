function ScoreController() {
    'use strict';

    var self = this;

    var passedBallAppear,
        errorAppear,
        fpsCounter;

    resetGlobals();
    function resetGlobals() {
        resetState();
    }

    function resetState() {
        passedBallAppear = false;
        errorAppear = false;
        fpsCounter = 0;
    }

    function addPitch(scoretype) {
        self.innings.addPitchScore(scoretype);
    }

    function addBatter(batter, scoretype) {
        self.innings.addBatterScore(batter, scoretype);
    }

    function addFielder(fielder, scoretype) {
        self.innings.addFielderScore(fielder, scoretype);
    }

    /** @type InningController */
    self.innings = null;
    /** @type PlayerController */
    self.players = null;

    self.on4Balls = function () {
        addPitch('BB');
        addBatter('BB');
    };


    self.onPitch = function (type) {
        addPitch('PC');
        addBatter('PC');

        switch (type) {
            case 'KC':
                addPitch('PCS');
                break;
            case 'KS':
                addPitch('PSS');
                break;
            case 'foul':
            case 'ball':
                //case 'HBP':
                //case 'BK':
                //case 'WP':
                //case 'PB':
                addPitch('PFB');
                break;
            case '1B':
            case '2B':
            case '3B':
            case 'HR':

            case 'F':
            case 'LO':
            case 'PO':
            case 'FBO':
            case 'IFR':

            case 'GO':
            case 'SF':
            case 'SH':
            case 'FO':
            case 'TO':
            case 'DP':
            case 'TP':
            case 'CS':
            case 'SB':
                addPitch('PIP');
                break;
        }

        fpsCounter++;
    };

    self.onAddInningScore = function (batter) {
        addBatter(batter, 'R');
        addPitch('R');
        addBatter('RBI');

        if (!passedBallAppear && !errorAppear) {
            addPitch('ER');
        }
    };

    self.onBatterHit = function (type) {
        addBatter('AB');
        addBatter('H');
        addPitch('H');
        addPitch('AB');

        if (type == 'HR') {
            addBatter('HR');
            addPitch('HR');
        }

        if (type == '2B' || type == '3B') {
            addBatter('v' + type);
            addPitch('v' + type);
        }
    };

    self.onPitchError = function (type) {
        var catcher;
        if (type == 'HBP') {
            addPitch('HB');
            addBatter('HBP');
        }

        if (type == 'PB') {
            passedBallAppear = true;

            catcher = self.players.getCurrentFielder(2);
            addFielder(catcher, 'PB');
        }

        if (type == 'WP') {
            catcher = self.players.getCurrentFielder(2);
            addFielder(catcher, 'C_WP');
            addPitch('WP');
        }

        fpsCounter = 0;
    };

    self.onStrikeOut = function (type) {
        addPitch('SO');
        addBatter('SO');
        addBatter('AB');
        addPitch('AB');
        addPitch('IP');

        for (var i = 0; i < fpsCounter; i++) {
            addPitch('FPS');
        }

        var catcher = self.players.getCurrentFielder(2);
        addFielder(catcher, 'PO');
    };

    self.onOut = function (type, fielders, batters) {
        if (type == 'GO') {
            addPitch('GO');
        }

        if (type == 'F') {
            addPitch('AO');
        }

        if (type == 'LO' ||
            type == 'PO' ||
            type == 'FBO' ||
            type == 'IFR') {
            addPitch('AO');
            addPitch(type);
        }

        if (type != 'SF' && type != 'SH') {
            addBatter('AB');
            addPitch('AB');
        } else {
            addBatter(type);
        }

        if (type == 'SF') {
            addPitch('SF');
        }

        // TODO: Wrong logic: fielder makes error but do PO
        var i;
        for (i = fielders.length - 1; i >= 0; i--) {
            var fielder = self.players.getCurrentFielder(fielders[i]);
            if (i < fielders.length - batters.length) {
                addFielder(fielder, 'A');
            } else {
                addFielder(fielder, 'PO');
            }

            if (batters.length == 2) {
                addFielder(fielder, 'DP');
            }

            if (batters.length == 3) {
                addFielder(fielder, 'TP');
            }
        }


        for (i = 0; i < batters.length; i++) {
            addPitch('IP');
        }
    };

    self.onMiscOut = function (batterId) {
        var batter = self.players.getPlayer(batterId, 'batters');
        addBatter(batter, 'CS');
        addPitch('CS');
        addPitch('IP');
    };

    self.onMiscBatterBase = function (batterId) {
        var batter = self.players.getPlayer(batterId, 'batters');
        addBatter(batter, 'SB');
        addPitch('SB');
    };

    self.onBatterReady = function () {
        addBatter('PA');
        addPitch('BF');
        resetState();
    };

    self.onError = function (obj) {
        var fielder = self.players.getCurrentFielder(obj.position);
        addFielder(fielder, 'E');
        errorAppear = true;
    };

    self.onBeforeNextGameSet = function () {
        var fielders = self.players.getCurrentPlayers('fielders');
        for (var f in fielders) {
            addFielder(fielders[f], 'INN');
        }
    };
}