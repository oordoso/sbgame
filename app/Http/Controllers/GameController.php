<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\Settings;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class GameController extends Controller
{
    private $umpires = [
        'Plateump' => 'Plate',
        'Fieldump1' => '1B',
        'Fieldump2' => '2B',
        'Fieldump3' => '3B',
        'Fieldump4' => 'LF line',
        'Fieldump5' => 'RF line'
    ];

    public function __construct()
    {
    }

    public function index()
    {
        $games = Game::whereRaw('date >= ? and date < ? and status <> -1', [Carbon::today(), Carbon::tomorrow()])->get();
        return view('game.admin',compact('games'));
    }

    public function edit($id)
    {
        $game = Game::findOrFail($id);
        $numberUmps = Settings::get()->numberUmps;
        $umpires = $this->umpires;
        return view('game.update.gameinfo',compact(['game','numberUmps','umpires']));
    }

    public function store($id, Request $request)
    {
        $game = Game::findOrFail($id);
        $numberUmps = Settings::get()->numberUmps;

        $conditions = [
            'status'=>'required|numeric',
            'attendance'=>'numeric',
            'weather'=>'required',
            'temperature'=>'required|numeric',
            'numberUmps'=>'required|numeric|min:'.$numberUmps,
        ];


        $input = $request->all();

        $numberUmpsInput = 0;
        foreach($this->umpires as $k => $v){
            $in = trim($input[$k]);
            if (!empty($in)){
                $numberUmpsInput++;
            }
        }

        $input['numberUmps'] = $numberUmpsInput;

        $validator = validator($input,$conditions);
        $validator->setAttributeNames($this->umpires);

        $umpires = $this->umpires;
        if ($validator->fails()){
            return view('game.update.gameinfo',compact(['game','numberUmps','umpires']))
                ->withErrors($validator);
        }

        if ($input['status'] == 0)
            $input['last_inning'] = 0;

        $game->update(array_except($input,['_token']));

        if (isset($input['redirect']))
            return redirect($input['redirect']);

        return redirect(action('LineupController@edit',['id'=>$id, 'team'=>'home']));
    }
}
