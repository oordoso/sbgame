<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AtBat;
use App\Models\Game;
use App\Models\Batter;
use App\Models\Settings;
use App\Helpers\MailHelper;
use Request;
use Response;

class AtBatApiController extends Controller
{
    public function postGameStatus()
    {
        $data = Request::all();
        $game = Game::findOrFail($data['_id']);
        $game->status = $data['data']['status'];
        $game->save();
        return Response::json(['success' => true]);
    }

    public function getGameStatus()
    {
        $data = Request::all();
        $game = Game::findOrFail($data['_id']);

        return Response::json(['status' => 0 + $game->status]);
    }

    public function postStorage()
    {
        $data = Request::all();
        $game = Game::findOrFail($data['_id']);

        $storage = $game->storage;
        if ($storage == null) {
            $storage = new AtBat();
            $storage->game()->associate($game);
            $storage->save();
        }

        $storage->storage = $data['data']['storage'];
        $storage->save();

        StatisticController::storeStatistic($game);

        return Response::json(['success' => true]);
    }

    public function getStorage()
    {
        $data = Request::all();
        $game = Game::findOrFail($data['_id']);

        $storage = $game->storage;
        if ($storage == null) {
            $storage = new AtBat();
            $storage->game()->associate($game);
            $storage->save();
        }

        return Response::json(['storage' => $storage->storage]);
    }

    public function postLastInning()
    {
        $data = Request::all();
        $game = Game::findOrFail($data['_id']);

        $game->last_inning = $data['data']['inning'];
        $game->save();

        return Response::json(['success' => true]);
    }

    public static function sendExceededNotification($game, $pitcher, $email, $keyWord)
    {
        $gameDate = $game->getDate()->setTimezone('EST')->format('m/d/Y h:i A T');
        $visitorName = $game->teamVisitor->Name;
        $homeName = $game->teamHome->Name;
        $pitcherName = $pitcher->player->getFullName();
        $message = "Game on $gameDate between $visitorName and $homeName Pitcher $pitcherName exceeded $keyWord PC setting";

        MailHelper::send($email, $keyWord.' PC exceeded',$message);
    }

    public function postPitcherPCLimit()
    {
        $data = Request::all();
        $game = Game::findOrFail($data['_id']);

        $pitcherId = $data['data']['id'];
        $limitType = $data['data']['type'];

        $pitcher = Batter::findOrFail($pitcherId);
        $settings = Settings::get();

        if ($limitType == 'innPCLimit'){
            if ($settings->innPCLimit == null || empty($settings->innPCEmail)){
                return Response::json(['success' => false, 'message'=>'Invalid general settings']);
            }

            if ($pitcher->innPCLimit > 0){
                return Response::json(['success' => false, 'message'=>'Invalid pitcher']);
            }

            self::sendExceededNotification($game, $pitcher, $settings->innPCEmail, 'INNINGS');

            $pitcher->innPCLimit = 1;
        }

        if ($limitType == 'totalPCLimit'){
            if ($settings->totalPCLimit == null || empty($settings->totalPCEmail)){
                return Response::json(['success' => false, 'message'=>'Invalid general settings']);
            }

            if ($pitcher->totalPCLimit > 0){
                return Response::json(['success' => false, 'message'=>'Invalid pitcher']);
            }

            self::sendExceededNotification($game, $pitcher, $settings->totalPCEmail, 'TOTAL');

            $pitcher->totalPCLimit = 1;
        }

        $pitcher->save();

        return Response::json(['success' => true]);
    }


}
