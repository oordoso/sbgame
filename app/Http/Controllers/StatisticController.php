<?php

namespace App\Http\Controllers;

use App\Models\Batter;
use App\Models\Lineup;
use App\Models\Player;
use DB;
use App\Models\_Season;
use App\Models\Game;
use App\Models\Team;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StatisticController extends Controller
{
    private $statTables = [
        'batting' => 'statshitting',
        'fielding' => 'statsfielding',
        'pitching' => 'statspitching',
    ];

    private function getLineup(Game $game, $team)
    {
        $lineup = ($team == 'home') ?
            $game->getLineupHome() :
            $game->getLineupVisitor();

        return $lineup;
    }

    private function getGameStatistic(Lineup $lineup, $table)
    {
        $res = DB::table($table)
            ->join('players', 'players.idplayer', '=', "$table.Players_idplayer")
            ->join('batters', 'batters.Players_idplayer', '=', 'players.idplayer')
            ->where("$table.Games_idgame", $lineup->game->idgame)
            ->where('batters.Lineup_idlineup', $lineup->idlineup)
            ->select(['batters.Number', 'players.Firstname', 'players.Lastname', "$table.*"])
            ->get();

        return $res;
    }

    private function getSeasonStatistic(_Season $season, Team $team, $table)
    {
        $select = '`batters`.`Players_idplayer`, `batters`.`Number`, `players`.`Firstname`, `players`.`Lastname`';
        $keys = self::getDbKeys($table);
        foreach ($keys as $key) {
            $select .= ", sum(`$table`.`$key`) as $key";
        }

        $res = DB::table($table)
            ->join('games', 'games.idgame', '=', "$table.Games_idgame")
            ->join('lineup', 'lineup.Games_idgame', '=', "$table.Games_idgame")
            ->join('players', 'players.idplayer', '=', "$table.Players_idplayer")
            ->join('batters', function ($join) {
                $join->on('batters.Players_idplayer', '=', 'players.idplayer')
                    ->on('batters.Lineup_idlineup', '=', 'lineup.idlineup');
            })
            ->where('games.season_idseason', '=', $season->idseason)
            ->where('lineup.Teams_idteam', $team->idteam)
            ->groupBy("$table.Players_idplayer")
            ->selectRaw($select)
            ->get();

        return $res;
    }

    private function getStatistic(Game $game, $team, $period, $type)
    {
        $lineup = $this->getLineup($game, $team);

        if ($lineup == null)
            return null;

        if ($period == 'season')
            return $this->getSeasonStatistic($game->season, $lineup->team, $this->statTables[$type]);
        else
            return $this->getGameStatistic($lineup, $this->statTables[$type]);
    }

    private function getTotals($stats, $keys)
    {
        $totals = [];
        foreach ($keys as $key) {
            $totals[$key] = 0;
            foreach ($stats as $stat) {
                $totals[$key] += $stat->$key;
            }
        }
        return $totals;
    }

    private static function getDbKeys($table)
    {
        switch ($table) {
            case 'statshitting':
                return ['G', 'AB', 'R', 'H', "v2B", "v3B", "HR", "RBI", "BB", "HBP", 'SO', 'SF', 'SH', 'SB', 'CS', 'DP', 'E', 'PA'];
            case 'statsfielding':
                return ["G", "GS", 'INN', 'PO', 'A', 'E'];
            case 'statspitching':
                return ['G', 'GS', 'CG', 'IP', 'H', 'R', 'ER', 'BB', 'SO', 'W', 'L', 'SV', 'v2B', 'v3B', 'BF', 'IBB', 'SHO', 'HR', 'WP', 'HB', 'PCS', 'PSS', 'PFB', 'PIP'];
        }
        return null;
    }

    private function getKeys($table)
    {
        switch ($table) {
//            case 'statshitting': return ["G", "AB", "R", "H", "v2B", "v3B", "HR", "RBI", "BB", "SO", "SB", "CS", "HBP", "SAC", "PA", "XBH", "AVG", "OBP", "SLG", "OPS"];
//            case 'statsfielding': return ["G", "GS", "INN", "TC", "PO", "A", "E", "DP", "SB", "CS", "SBPCT", "PB", "C_WP", "FPCT", "RF"];
//            case 'statspitching': return ["W", "L", "ERA", "G", "GS", "SV", "SVO", "IP", "H", "R", "ER", "HR", "BB", "SO", "HB", "CS", "TBF", "SB", "NP", "WPCT", "OBP", "SLG", "GO_AO", "OPS", "BB_9", "H_9", "K_9", "K_BB", "P_IP", "AVG", "WHIP"];
            // TODO: Wrong implementation: using of dynamic (calculable) field in DB
            case 'statshitting':
                return ['AVG', 'AB', 'R', 'H', "v2B", "v3B", "HR", "RBI", "BB", "HBP", 'SO', 'SF', 'SH', 'SB', 'CS', "OBP", "SLG", "OPS", 'PA', 'AB_HR', 'BB_PA', 'BB_K', 'SECA', 'ISOP', 'RC'];
            case 'statsfielding':
                return ["INN", 'TC', "PO", "A", "E", 'DP', 'TP', 'PB', 'C_WP', "FPCT", 'RF'];
            case 'statspitching':
                return ['IP', 'H', 'R', 'ER', 'BB', 'SO', 'v2B', 'v3B', 'ERA', 'BF', 'SHO', 'HR', 'BAA', 'WP', 'HB', 'WHIP', 'PFR', 'BIPA', 'K_9', 'ERC', 'PTB', 'FPSP'];
        }
        return null;
    }

    private static function compileGameScores(Game $game)
    {
        function addScore(&$data, $type, $playerId, $score)
        {
            if (!isset($data[$type]))
                $data[$type] = [];
            if (!isset($data[$type][$playerId]))
                $data[$type][$playerId] = [];
            if (!isset($data[$type][$playerId][$score]))
                $data[$type][$playerId][$score] = 0;
            $data[$type][$playerId][$score]++;
        }


        $data = json_decode($game->storage->storage, true);

        $playersIds = [];
        foreach ($game->lineups as $lineup) {
            foreach ($lineup->batters as $batter) {
                $playersIds[$batter->idbatter] = $batter->player->idplayer;
            }
        }

        $stats = [];
        foreach ($data['innings'] as $i => $inning) {
            foreach ($inning['state'] as $s => $state) {
                foreach ($state['playerScores'] as $score) {
                    switch ($score['type']) {
                        case 'pitchers':
                            addScore($stats, 'statspitching', $playersIds[$score['id']], $score['score']);
                            break;
                        case 'batters':
                            addScore($stats, 'statshitting', $playersIds[$score['id']], $score['score']);
                            break;
                        case 'fielders':
                            addScore($stats, 'statsfielding', $playersIds[$score['id']], $score['score']);
                            break;
                    }
                }
            }
        }

        // $stats = [type=>[pid=>[score=>0]]]
        return $stats;
    }

    // $stats [pid=>[score=>0]]
    private function completeGameScores(&$stats, $table)
    {

//        function calculatePitching(&$scores)
//        {
//            $IP = $scores['IP'] / 3;
//            $scores['ERA'] = $IP > 0 ? 9 * $scores['ER'] / $IP : -1;
//            $scores['WHIP'] = $IP > 0 ? ($scores['BB'] + $scores['H']) / $IP : -1;
//            $scores['PFR'] = $IP > 0 ? ($scores['SO'] + $scores['BB']) / $IP : -1;
//            $scores['K_9'] = $IP > 0 ? 9 * $scores['SO'] / $IP : -1;
//            $scores['BIPA'] = ($scores['AB'] - $scores['SO'] - $scores['HR'] + $scores['SF']) > 0 ?
//                ($scores['H'] - $scores['HR']) / ($scores['AB'] - $scores['SO'] - $scores['HR'] + $scores['SF']) : -1;
//            $scores['PTB'] = 0.89 * (1.255 * ($scores['H'] - $scores['HR']) + 4 * $scores['HR']) + 0.56 * ($scores['BB'] + $scores['HB'] - $scores['IBB']);
//            $scores['ERC'] = $scores['BF'] * $IP > 0 ? ((($scores['H'] + $scores['BB'] + $scores['HB']) * $scores['PTB']) / ($scores['BF'] * $IP)) * 9 - 0.56 : -1;
//        }
//
//        function calculateHitting(&$scores)
//        {
//            $scores['SLG'] = $scores['AB'] > 0 ? ($scores['H'] + $scores['v2B'] + 2 * $scores['v3B'] + 3 * $scores['HR']) / $scores['AB'] : -1;
//            $scores['OBP'] = ($scores['AB'] + $scores['BB'] + $scores['HBP'] + $scores['SF']) > 0 ?
//                ($scores['H'] + $scores['BB'] + $scores['HBP']) / ($scores['AB'] + $scores['BB'] + $scores['HBP'] + $scores['SF']) : -1;
//            $scores['OPS'] = $scores['SLG'] + $scores['OBP'];
//            $scores['AB_HR'] = $scores['HR'] > 0 ? $scores['AB'] / $scores['HR'] : -1;
//            $scores['BB_PA'] = $scores['PA'] > 0 ? $scores['BB'] / $scores['PA'] : -1;
//            $scores['BB_K'] = $scores['SO'] > 0 ? $scores['BB'] / $scores['SO'] : -1;
//            $scores['SECA'] = $scores['AB'] > 0 ?
//                ($scores['v2B'] + 2 * $scores['v3B'] + 3 * $scores['HR'] + $scores['BB'] + $scores['SB'] - $scores['CS']) / $scores['AB'] : -1;
//            $scores['ISOP'] = $scores['AB'] > 0 ? ($scores['v2B'] + 2 * $scores['v3B'] + 3 * $scores['HR']) / $scores['AB'] : -1;
//            $scores['RC'] = ($scores['AB'] + $scores['BB']) > 0 ?
//                ($scores['H'] + $scores['BB']) * ($scores['H'] + $scores['v2B'] + 2 * $scores['v3B'] + 3 * $scores['HR']) / ($scores['AB'] + $scores['BB']) : -1;
//        }
//
//        function calculateFielding(&$scores)
//        {
//            $scores['TC'] = $scores['A'] + $scores['PO'] + $scores['E'];
//            $scores['FPCT'] = $scores['TC'] > 0 ? ($scores['A'] + $scores['PO']) / $scores['TC'] : -1;
//        }

        function calculatePitching(&$scores)
        {
            $IP = $scores->IP / 3;
            $scores->ERA = $IP > 0 ? 9 * $scores->ER / $IP : 0;
            $scores->WHIP = $IP > 0 ? ($scores->BB + $scores->H) / $IP : 0;
            $scores->PFR = $IP > 0 ? ($scores->SO + $scores->BB) / $IP : 0;
            $scores->K_9 = $IP > 0 ? 9 * $scores->SO / $IP : 0;
            $scores->BIPA = ($scores->H + $scores->SO - $scores->SO - $scores->HR + $scores->SF) > 0 ?
                ($scores->H - $scores->HR) / ($scores->H + $scores->SO - $scores->SO - $scores->HR + $scores->SF) : 0;
            $scores->PTB = 0.89 * (1.255 * ($scores->H - $scores->HR) + 4 * $scores->HR) + 0.475 * ($scores->BB + $scores->HB);
            $scores->ERC = $scores->BF * $IP > 0 ? ((($scores->H + $scores->BB + $scores->HB) * $scores->PTB) / ($scores->BF * $IP)) * 9 - 0.56 : 0;
            $scores->ERC = $scores->BF * $IP > 0 && $scores->ERC < 2.24?
                (((($scores->H + $scores->BB + $scores->HB) * $scores->PTB) / ($scores->BF * $IP)) * 9) * 0.75:
                $scores->ERC;
            $scores->FPSP = $scores->PC >0? $scores->FPS / $scores->PC: 0;
            $scores->BAA = $scores->AB? $scores->H / $scores->AB: 0;

            $scores->IP = floor($scores->IP/3).'.'.($scores->IP%3);
        }

        function calculateHitting(&$scores)
        {
            $scores->AVG = $scores->AB > 0 ? $scores->H / $scores->AB: 0;
                $scores->SLG = $scores->AB > 0 ? ($scores->H + $scores->v2B + 2 * $scores->v3B + 3 * $scores->HR) / $scores->AB : 0;
            $scores->OBP = ($scores->AB + $scores->BB + $scores->HBP + $scores->SF) > 0 ?
                ($scores->H + $scores->BB + $scores->HBP) / ($scores->AB + $scores->BB + $scores->HBP + $scores->SF) : 0;
            $scores->OPS = $scores->SLG + $scores->OBP;
            $scores->AB_HR = $scores->HR > 0 ? $scores->AB / $scores->HR : 0;
            $scores->BB_PA = $scores->PA > 0 ? $scores->BB / $scores->PA : 0;
            $scores->BB_K = $scores->SO > 0 ? $scores->BB / $scores->SO : 0;
            $scores->SECA = $scores->AB > 0 ?
                ($scores->v2B + 2 * $scores->v3B + 3 * $scores->HR + $scores->BB + $scores->SB - $scores->CS) / $scores->AB : 0;
            $scores->ISOP = $scores->AB > 0 ? ($scores->v2B + 2 * $scores->v3B + 3 * $scores->HR) / $scores->AB : 0;
            $scores->RC = ($scores->AB + $scores->BB) > 0 ?
                ($scores->H + $scores->BB) * ($scores->H + $scores->v2B + 2 * $scores->v3B + 3 * $scores->HR) / ($scores->AB + $scores->BB) : 0;

            $scores->P = $scores->AB>0? $scores->PC / $scores->AB:0;
        }

        function calculateFielding(&$scores)
        {
            $scores->TC = $scores->A + $scores->PO + $scores->E;
            $scores->FPCT = $scores->TC > 0 ? ($scores->A + $scores->PO) / $scores->TC : 0;
            $scores->RF = $scores->INN >0? 9*($scores->A + $scores->PO)/$scores->INN:0;
        }
        
        $keys = $this->getKeys($table);

        foreach ($stats as $pid => $scores) {
            foreach ($keys as $score) {
                if (!isset($stats[$pid]->$score))
                    $stats[$pid]->$score = 0;
            }

            switch ($table) {
                case 'statshitting':
                    calculateHitting($stats[$pid]);
                    break;
                case 'statsfielding':
                    calculateFielding($stats[$pid]);
                    break;
                case 'statspitching':
                    calculatePitching($stats[$pid]);
                    break;
            }
        }


    }

    public static function resetStatistic(Game $game)
    {
        DB::table('statshitting')->where("Games_idgame", $game->idgame)->delete();
        DB::table('statsfielding')->where("Games_idgame", $game->idgame)->delete();
        DB::table('statspitching')->where("Games_idgame", $game->idgame)->delete();

        $lineups = $game->lineups;
        foreach ($lineups as $lineup) {
            $batters = $lineup->batters;
            $hasHD = Lineup::hasDH($batters);
            foreach (Lineup::takeHitters($batters, $hasHD) as $batter) {
                DB::table('statshitting')->insert(['Players_idplayer' => $batter->Players_idplayer, 'Games_idgame' => $game->idgame]);
            }
            foreach (Lineup::takeFielders($batters, $hasHD) as $batter) {
                DB::table('statsfielding')->insert(['Players_idplayer' => $batter->Players_idplayer, 'Games_idgame' => $game->idgame]);
            }
            foreach (Lineup::takePitchers($batters) as $batter) {
                DB::table('statspitching')->insert(['Players_idplayer' => $batter->Players_idplayer, 'Games_idgame' => $game->idgame]);
            }
        }
    }

    public static function storeStatistic(Game $game)
    {
        $clearScores = function ($table) use ($game){
            $values = [];

            foreach(self::getDbKeys($table) as $key)
                $values[$key] = 0;

            DB::table($table)->where('Games_idgame', $game->idgame)->update($values);
        };

        $clearScores('statshitting');
        $clearScores('statsfielding');
        $clearScores('statspitching');

        $stats = self::compileGameScores($game);
        foreach($stats as $table=>$players){
            foreach($players as $pid=>$scores){
                DB::table($table)
                    ->where('Games_idgame', $game->idgame)
                    ->where('Players_idplayer', $pid)
                    ->update($scores);
            }
        }
    }

    private function getPlayerInfo($player)
    {
        $class = [0 => 'Freshman', 1 => 'Sophomore', 2 => 'Junior', 3 => 'Senior', 4 => 'Grad School'];
        return [
            'id' => $player->idplayer,
            'number' => $player->Number,
            'birthdate' => $player->Birthdate,
            'name' => $player->getFullName(),
            'bats_throws' => $player->Bats . '/' . $player->Throws,
            'height' => $player->Height,
            'weight' => $player->Weight,
            'college' => $player->College,
            'class' => $class[$player->Class],
            'hometown' => !empty($player->Hometown) ? $player->Hometown . (!empty($player->State) ? ', ' . $player->State : '') : '',
            'image' => ''
        ];
    }

    private function getRoster($team)
    {
        $result = [
            'pitchers' => [],
            'catchers' => [],
            'infielders' => [],
            'outfielders' => [],
            'staff' => []
        ];

        $positions = [
            'P' => 'pitchers', 'C' => 'catchers',
            '1B' => 'infielders', '2B' => 'infielders', '3B' => 'infielders', 'SS' => 'infielders', 'DH' => 'infielders',
            'LF' => 'outfielders', 'CF' => 'outfielders', 'RF' => 'outfielders',
            'EF' => 'staff', 'PH' => 'staff',
            'PR' => 'staff', 'CR' => 'staff',
            'MG' => 'staff', 'AC' => 'staff',
            'BC' => 'staff', 'PC' => 'staff'
        ];

        $players = $team->players;
        foreach ($players as $player) {
            $result[$positions[$player->Position]][] = $this->getPlayerInfo($player);
        }

        return $result;
    }

    private function getPlayerStatistic($player, $period)
    {
    }

    public function stats($id, $team, $period, $type)
    {
        $game = Game::findOrFail($id);
        $stats = $this->getStatistic($game, $team, $period, $type);

        if ($stats == null)
            return view('game.update.statistics.stats', compact(['game', 'team', 'period', 'type', 'stats']))
                ->withErrors(['Lineup is not created.']);

        $this->completeGameScores($stats, $this->statTables[$type]);

        $keys = $this->getKeys($this->statTables[$type]);
        $totals = $this->getTotals($stats, $keys);
        return view('game.update.statistics.stats', compact(['game', 'team', 'period', 'type', 'stats', 'keys', 'totals']));
    }

    public function roster($id, $team)
    {
        $game = Game::findOrFail($id);
        $lineup = $this->getLineup($game, $team);
        if ($lineup == null)
            return null;
        $roster = $this->getRoster($lineup->team);

        return view('game.update.statistics.roster', compact(['game', 'team', 'roster']));
    }

    public function player($id, $player, $period)
    {
        $game = Game::findOrFail($id);
        $player = Player::findOrFail($player);
        $player = $this->getPlayerInfo($player);

        return view('game.update.statistics.player', compact(['game', 'player', 'period']));
    }
}
