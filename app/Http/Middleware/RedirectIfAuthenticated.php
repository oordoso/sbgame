<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // Fixme: Auth::guard($guard)->check() == true but Session::get('login.time') == null
        if (Auth::guard($guard)->check() && Session::get('login.time') != null) {
            return redirect('/');
        }

        if (Auth::guard($guard)->check()) {
            Auth::logout();
        }

        return $next($request);
    }
}
