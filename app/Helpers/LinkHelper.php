<?php
/**
 * Created by PhpStorm.
 * User: VIRT
 * Date: 25.05.2016
 * Time: 13:06
 */

namespace App\Helpers;

use Illuminate\Support\Facades\File;

class LinkHelper {
    public static function publicResource($path)
    {

        return $path.'?v='.File::lastModified(public_path().$path);
    }
}