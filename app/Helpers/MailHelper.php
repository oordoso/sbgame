<?php
/**
 * Created by PhpStorm.
 * User: VIRT
 * Date: 01.06.2016
 * Time: 14:05
 */

namespace App\Helpers;

use Mail;
use Request;


class MailHelper {
    public static function send($receiverEmail, $subject, $message)
    {
        $from = 'support@'.Request::getHttpHost();
        $fromName="Northwood League Support";

        $fromName='=?UTF-8?B?'.base64_encode($fromName).'?=';
        $subject='=?UTF-8?B?'.base64_encode($subject).'?=';
        $headers="From: $fromName <{$from}>\r\n".
            "MIME-Version: 1.0\r\n".
            "Content-type: text/html; charset=UTF-8";

        mail($receiverEmail,$subject,$message,$headers);

//        Mail::raw($message, function ($m) use($receiverEmail, $subject){
//            $m->from('support@'.Request::getHttpHost(), "Northwood League Support");
//            $m->to($receiverEmail)->subject($subject);
//        });
    }
}